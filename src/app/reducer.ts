import { combineReducers } from '@reduxjs/toolkit'
import player from '../features/player/playerSlice'
import search from '../features/search/searchSlice'
import mediaLibrary from '../features/mediaLibrary/mediaLibrarySlice'
import { reducer as formReducer } from 'redux-form'

const reducer = combineReducers({
  player,
  search,
  mediaLibrary,
  form: formReducer,
})

export type RootState = ReturnType<typeof reducer>

export default reducer
