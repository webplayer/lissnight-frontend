import React, { useEffect } from 'react'
import 'antd/dist/antd.css'
import './App.scss'
import LayoutSwitcher from '../features/router/LayoutSwitcher'
import PlayerCore from '../features/player/PlayerCore'
import { getMediaLibrary } from '../features/mediaLibrary/mediaLibrarySlice'
import { Route, Switch } from 'react-router-dom'
import LoginPageContainer from '../features/auth/containers/LoginPageContainer'
import { observer } from 'mobx-react'
import auth from '../features/auth/auth'
import { useDispatch } from 'react-redux'

const App = observer(() => {
  const dispatch = useDispatch()

  useEffect(() => {
    auth.checkStatus()
  })

  useEffect(() => {
    dispatch(getMediaLibrary())
  }, [auth.status])

  return (
    <Switch>
      <Route path="/login">
        <LoginPageContainer />
      </Route>
      <Route path="/">
        <LayoutSwitcher />
        <PlayerCore />
      </Route>
    </Switch>
  )
})

export default App
