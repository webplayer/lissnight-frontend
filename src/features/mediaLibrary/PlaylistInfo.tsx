import React, { useCallback, useState } from 'react'
import { Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { SmartImage } from '../../components/LazyImage'
import styles from './PlaylistInfo.module.scss'
import clsx from 'clsx'

type PlaylistInfoProps = {
  title: string
  image: string
}

const useStyles = makeStyles((theme) => ({
  Container: (props: { backgroundColor: string }) => ({
    // backgroundColor: props.backgroundColor,
  }),
  Background: (props: { backgroundColor: string }) => ({
    backgroundColor: props.backgroundColor + '!important',
  }),
  Image: {
    height: '180px',
    zIndex: 1,
    boxShadow:
      '0 3px 5px -1px rgba(0, 0, 0, 0.2), 0 5px 8px 0 rgba(0, 0, 0, 0.14), 0 1px 14px 0 rgba(0, 0, 0, 0.12)',
    borderRadius: '4px',
    [theme.breakpoints.down('sm')]: {
      height: '120px',
    },
    flex: '0 0 auto',
  },
  Text: {
    color: '#b3b3b3',
    textAlign: 'left',
    paddingTop: '25px',
    zIndex: 1,
    paddingLeft: '10px',
  },
}))

const PlaylistInfo = ({ title, image }: PlaylistInfoProps) => {
  const [dominantColor, setDominantColor] = useState<string>('transparent')
  const classes = useStyles({ backgroundColor: dominantColor })

  const getDominantColor = useCallback(
    (colors: string[]) => {
      if (colors.length) {
        setDominantColor(colors[0])
      }
    },
    [image]
  )

  return (
    <div className={clsx(classes.Container, styles.Container)}>
      <SmartImage src={image} className={classes.Image} />
      <Typography variant="h4" className={classes.Text}>
        {title}
      </Typography>
      {/*<ColorExtractor src={image} getColors={getDominantColor} />*/}
      {/*<div className={clsx(classes.Background, styles.Background)} />*/}
    </div>
  )
}

export default PlaylistInfo
