import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import CellsGrid from '../../components/CellsGrid'
import { useHistory, useParams } from 'react-router-dom'
import {
  getCurrentPlaylist,
  getCurrentPlaylistId,
  getDynamicPlaylistBy,
  getIsPlaying,
} from '../../utils/Selectors'
import AudioCell from '../player/AudioCell'
import { getDynamicPlaylist } from './mediaLibrarySlice'
import { Audio, Playlist } from '../player/types'
import { setPlaylist } from '../player/playerSlice'
import MediaCell from '../../components/MediaCell'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Typography,
} from '@material-ui/core'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import PlaylistInfo from './PlaylistInfo'
import { RootState } from '../../app/reducer'

const DynamicPlaylist = () => {
  const { id, source } = useParams<{ id: string; source: 'vk' | 'youtube' }>()

  const isLoading = useSelector(
    (state: RootState) => state.mediaLibrary.isLoading
  )

  const playlist = useSelector((state: RootState) =>
    getDynamicPlaylistBy(state, id, source)
  )

  const dispatch = useDispatch()
  useEffect(() => {
    if (!isLoading) {
      dispatch(getDynamicPlaylist(id, source))
    }
  }, [id, source, isLoading])

  const onPlaylistSet = (audio: Audio) => {
    if (playlist) {
      const newPlaylist: Playlist = {
        id: playlist.id,
        url: `/playlist/${id}`,
        name: playlist.title,
        audios: playlist.audios,
        isDynamic: true,
      }
      dispatch(setPlaylist({ playlist: newPlaylist, audio }))
    }
  }

  const audioCells =
    playlist &&
    playlist.audios &&
    [...playlist.audios].map((el: Audio) => (
      <AudioCell
        key={el.id}
        audio={el}
        onPlaylistSet={onPlaylistSet}
        playlistId={playlist.id}
      />
    ))

  const currentPlaylistId = useSelector(getCurrentPlaylistId)
  const currentPlaylist = useSelector(getCurrentPlaylist)
  const [currentMediaPlaylist, setCurrentMediaPlaylist] = useState({
    id: currentPlaylistId,
    isDynamic: currentPlaylist.isDynamic,
  })

  const isPlaying = useSelector(getIsPlaying)
  const history = useHistory()

  const subPlaylistsCells =
    playlist &&
    playlist.playlists &&
    playlist.playlists.map((playlist) => {
      const onPlaylistPlay = () => {
        history.push(
          `/playlist/dynamic/${playlist.source}/${playlist.external_id}`
        )
      }
      const onPlaylistOpen = () => {
        history.push(
          `/playlist/dynamic/${playlist.source}/${playlist.external_id}`
        )
      }
      return (
        <MediaCell
          key={playlist.id}
          isPlaying={isPlaying && currentMediaPlaylist.id === playlist.id}
          thumbnail={playlist.thumbnail}
          onButtonClick={onPlaylistPlay}
          onCellClick={onPlaylistOpen}
          info={playlist.title}
        />
      )
    })

  return (
    <>
      {playlist && playlist.title && (
        <PlaylistInfo title={playlist.title} image={playlist.thumbnail} />
      )}
      {playlist && playlist.playlists && (
        <div
          style={{
            backgroundColor: 'rgba(255, 255, 255, 0.16)',
            borderRadius: '5px',
            marginTop: '25px',
          }}
        >
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography
                variant="h5"
                style={{
                  color: '#b3b3b3',
                  textAlign: 'left',
                  paddingLeft: '15px',
                }}
              >
                Плейлисты
              </Typography>
            </AccordionSummary>
            <AccordionDetails style={{ display: 'block' }}>
              <CellsGrid paddingTop="0">{subPlaylistsCells}</CellsGrid>
            </AccordionDetails>
          </Accordion>
        </div>
      )}
      <CellsGrid>{audioCells}</CellsGrid>
    </>
  )
}

export default DynamicPlaylist
