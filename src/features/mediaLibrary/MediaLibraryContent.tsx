import React, { useEffect, useState } from 'react'
import {
  getCurrentPlaylist,
  getCurrentPlaylistId,
  getIsPlaying,
  getMediaLibraryPlaylists,
} from '../../utils/Selectors'
import { useDispatch, useSelector } from 'react-redux'
import CellsGrid from '../../components/CellsGrid'
import MediaCell from '../../components/MediaCell'
import { useHistory } from 'react-router-dom'
import { getStaticPlaylist } from './mediaLibrarySlice'
import { Playlist } from '../player/types'
import { setPlaying, setPlaylist } from '../player/playerSlice'

const MediaLibraryContent = () => {
  const playlists = useSelector(getMediaLibraryPlaylists)

  const history = useHistory()
  const dispatch = useDispatch()

  const currentPlaylistId = useSelector(getCurrentPlaylistId)
  const currentPlaylist = useSelector(getCurrentPlaylist)
  const [currentMediaPlaylist, setCurrentMediaPlaylist] = useState({
    id: currentPlaylistId,
    isDynamic: currentPlaylist.isDynamic,
  })

  useEffect(() => {
    setCurrentMediaPlaylist({
      id: currentPlaylistId,
      isDynamic: currentPlaylist.isDynamic,
    })
  }, [currentPlaylistId])

  useEffect(() => {
    if (currentMediaPlaylist.isDynamic) {
    } else {
      dispatch(getStaticPlaylist(currentMediaPlaylist.id))
    }
  }, [currentMediaPlaylist])

  useEffect(() => {
    if (currentPlaylist.id !== currentMediaPlaylist.id) {
      const mediaPlaylist = playlists.find(
        (playlist) => playlist.id === currentMediaPlaylist.id
      )
      if (mediaPlaylist?.audios.length) {
        const newPlaylist: Playlist = {
          id: mediaPlaylist.id,
          url: `/playlist/${mediaPlaylist.id}`,
          name: mediaPlaylist.title,
          audios: mediaPlaylist.audios,
          isDynamic: mediaPlaylist.dynamic,
        }
        dispatch(
          setPlaylist({ playlist: newPlaylist, audio: newPlaylist.audios[0] })
        )
      }
    }
  }, [playlists, currentMediaPlaylist.id])

  const isPlaying = useSelector(getIsPlaying)

  const renderPlaylists = playlists.map((playlist) => {
    const onPlaylistPlay = () => {
      setCurrentMediaPlaylist({
        id: playlist.id,
        isDynamic: playlist.dynamic,
      })
      if (currentMediaPlaylist.id === playlist.id) {
        dispatch(setPlaying(!isPlaying))
      }
    }
    const onPlaylistOpen = () => {
      if (playlist.dynamic) {
        history.push(
          `/playlist/dynamic/${playlist.source}/${playlist.external_id}`
        )
      } else {
        history.push(`/playlist/${playlist.id}`)
      }
    }
    return (
      <MediaCell
        key={playlist.id}
        isPlaying={isPlaying && currentMediaPlaylist.id === playlist.id}
        thumbnail={playlist.thumbnail}
        onButtonClick={onPlaylistPlay}
        onCellClick={onPlaylistOpen}
        info={playlist.title}
      />
    )
  })

  return <CellsGrid>{renderPlaylists}</CellsGrid>
}

export default MediaLibraryContent
