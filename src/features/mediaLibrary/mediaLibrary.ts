import { makeAutoObservable } from 'mobx'
import MediaLibraryApi, {
  MediaLibraryDynamicPlaylist,
  MediaLibraryPlaylist,
} from '../../api/MediaLibraryApi'
import { Audio } from '../player/types'

class MediaLibrary {
  dynamic: Array<MediaLibraryDynamicPlaylist> = []
  static: Array<MediaLibraryPlaylist> = []
  favorite: Array<Audio> = []
  isLoading: boolean = false

  constructor() {
    makeAutoObservable(this)
  }

  getMediaLibrary() {
    this.isLoading = true
    MediaLibraryApi.getMediaLibrary()
      .then((res) => {
        this.isLoading = false
        this.dynamic = res.dynamic
        this.static = res.static
        this.favorite = res.favorite
      })
      .catch(() => {
        this.isLoading = false
      })
  }

  getDynamicPlaylist(id: string, source: 'vk' | 'youtube') {
    MediaLibraryApi.getDynamicPlaylist(id, source).then((res) => {
      let targetPlaylistIdx = this.static.findIndex((el) => {
        return el.id.toString() === res.id.toString()
      })
      if (targetPlaylistIdx !== null) {
        this.dynamic[targetPlaylistIdx] = res
      } else {
        this.dynamic.push(res)
      }
    })
  }

  getStaticPlaylist(id: string) {
    this.isLoading = true
    MediaLibraryApi.getStaticPlaylist(id)
      .then((res) => {
        let targetPlaylistIdx = this.static.findIndex((el) => {
          return el.id.toString() === res.id.toString()
        })
        if (targetPlaylistIdx !== null) {
          this.static[targetPlaylistIdx] = res
        }
      })
      .finally(() => {
        this.isLoading = false
      })
  }
}

export default new MediaLibrary()
