import React, { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../app/reducer'
import CellsGrid from '../../components/CellsGrid'
import AudioCell from '../player/AudioCell'
import { getStaticPlaylist } from './mediaLibrarySlice'
import { Audio, Playlist } from '../player/types'
import { setPlaylist } from '../player/playerSlice'
import { getStaticPlaylistById } from '../../utils/Selectors'

const StaticPlaylist = () => {
  const { id } = useParams<{ id: string }>()

  const playlist = useSelector((state: RootState) =>
    getStaticPlaylistById(state, id)
  )

  const dispatch = useDispatch()

  useEffect(() => {
    if (playlist && !playlist.audios.length) {
      dispatch(getStaticPlaylist(id))
    }
  }, [playlist, id, dispatch])

  const onPlaylistSet = (audio: Audio) => {
    if (playlist) {
      const newPlaylist: Playlist = {
        id: playlist.id,
        url: `/playlist/${id}`,
        name: playlist.title,
        audios: playlist.audios,
        isDynamic: false,
      }
      dispatch(setPlaylist({ playlist: newPlaylist, audio }))
    }
  }

  const audioCells =
    playlist &&
    playlist.audios.map((el) => (
      <AudioCell
        key={el.id}
        audio={el}
        onPlaylistSet={onPlaylistSet}
        playlistId={playlist.id}
      />
    ))

  return <CellsGrid>{audioCells}</CellsGrid>
}

export default StaticPlaylist
