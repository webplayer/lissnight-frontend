import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { Audio } from '../player/types'
import { AppDispatch, AppThunk } from '../../app/store'
import MediaLibraryApi, {
  MediaLibraryDynamicPlaylist,
  MediaLibraryPlaylist,
} from '../../api/MediaLibraryApi'

type MediaLibrary = {
  dynamic: Array<MediaLibraryDynamicPlaylist>
  static: Array<MediaLibraryPlaylist>
  favorite: Array<Audio>
  isLoading: boolean
}

const initialState: MediaLibrary = {
  dynamic: [],
  static: [],
  favorite: [],
  isLoading: false,
}

const mediaLibrarySlice = createSlice({
  name: 'mediaLibrary',
  initialState,
  reducers: {
    getMediaLibraryRequest(state) {
      state.isLoading = true
    },
    getMediaLibrarySuccess(state, action: PayloadAction<MediaLibrary>) {
      state.isLoading = false
      state.dynamic = action.payload.dynamic
      state.static = action.payload.static
      state.favorite = action.payload.favorite
    },
    getDynamicPlaylistRequest(state) {
      // state.isLoading = true
    },
    getDynamicPlaylistSuccess(
      state,
      action: PayloadAction<MediaLibraryDynamicPlaylist>
    ) {
      // state.isLoading = false
      let targetPlaylistIdx = state.static.findIndex((el) => {
        return el.id.toString() === action.payload.id.toString()
      })
      if (targetPlaylistIdx !== null) {
        state.dynamic[targetPlaylistIdx] = action.payload
      } else {
        state.dynamic.push(action.payload)
      }
    },
    getStaticPlaylistRequest(state) {
      state.isLoading = true
    },
    getStaticPlaylistSuccess(
      state,
      action: PayloadAction<MediaLibraryPlaylist>
    ) {
      state.isLoading = false
      let targetPlaylistIdx = state.static.findIndex((el) => {
        return el.id.toString() === action.payload.id.toString()
      })
      if (targetPlaylistIdx !== null) {
        state.static[targetPlaylistIdx] = action.payload
      }
    },
  },
})

export const getMediaLibrary = (): AppThunk => async (
  dispatch: AppDispatch
) => {
  dispatch(mediaLibrarySlice.actions.getMediaLibraryRequest())
  MediaLibraryApi.getMediaLibrary()
    .then((res) => {
      dispatch(
        mediaLibrarySlice.actions.getMediaLibrarySuccess({
          ...res,
          isLoading: false,
        })
      )
    })
    .catch(null)
}

export const getDynamicPlaylist = (
  id: string,
  source: 'vk' | 'youtube'
): AppThunk => async (dispatch: AppDispatch) => {
  dispatch(mediaLibrarySlice.actions.getDynamicPlaylistRequest())
  MediaLibraryApi.getDynamicPlaylist(id, source).then((res) => {
    // @ts-ignore
    dispatch(mediaLibrarySlice.actions.getDynamicPlaylistSuccess(res))
  })
}

export const getStaticPlaylist = (id: string): AppThunk => async (
  dispatch: AppDispatch
) => {
  dispatch(mediaLibrarySlice.actions.getStaticPlaylistRequest())
  MediaLibraryApi.getStaticPlaylist(id).then((res) => {
    dispatch(mediaLibrarySlice.actions.getStaticPlaylistSuccess(res))
  })
}

export default mediaLibrarySlice.reducer
