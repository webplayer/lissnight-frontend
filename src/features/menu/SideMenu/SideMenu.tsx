import React, { ChangeEvent, useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import {
  Button,
  Divider,
  Drawer,
  Input,
  List,
  ListSubheader,
} from '@material-ui/core'
import styles from './SideMenu.module.scss'
import './SideMenu.scss'
import HomeRoundedIcon from '@material-ui/icons/HomeRounded'
import SearchRoundedIcon from '@material-ui/icons/SearchRounded'
import QueueMusicRoundedIcon from '@material-ui/icons/QueueMusicRounded'
import AddBoxRoundedIcon from '@material-ui/icons/AddBoxRounded'
import FavoriteRoundedIcon from '@material-ui/icons/FavoriteRounded'
import VolumeUpRoundedIcon from '@material-ui/icons/VolumeUpRounded'
import HistoryRoundedIcon from '@material-ui/icons/HistoryRounded'
import MenuItem from './MenuItem'
import { useDispatch, useSelector } from 'react-redux'
import { SmartImage } from '../../../components/LazyImage'
import {
  getMediaLibraryPlaylists,
  getName,
  getRoom,
} from '../../../utils/Selectors'
import { setRoom } from '../../player/playerSlice'
import auth from '../../auth/auth'
import { observer } from 'mobx-react'
import mediaLibrary from '../../mediaLibrary/mediaLibrary'

type SideMenuProps = {
  isOpen: boolean
}

const SideMenu: React.FC<SideMenuProps> = observer(({ isOpen }) => {
  const [selectedIndex, setSelectedIndex] = React.useState(0)
  const handleListItemClick = (index: React.SetStateAction<number>) => {
    setSelectedIndex(index)
  }

  const history = useHistory()

  const playlists = useSelector(getMediaLibraryPlaylists)

  const onHomeClick = () => {
    handleListItemClick(0)
    history.push('/')
  }

  const onSearchClick = () => {
    handleListItemClick(1)
    history.push('/search')
  }

  const onPlaylistClick = (
    id: string,
    dynamic: boolean,
    externalId?: string,
    source?: string
  ) => {
    if (dynamic && externalId && source) {
      onDynamicPlaylistClick(externalId, source)
    } else {
      onStaticPlaylistClick(id)
    }
  }

  const onDynamicPlaylistClick = (id: string, source: string) => {
    history.push(`/playlist/dynamic/${source}/${id}`)
  }
  const onStaticPlaylistClick = (id: string) => {
    history.push(`/playlist/${id}`)
  }

  const onNowPlayingClick = () => {
    history.push('/now-playing')
  }

  const onMediaCollectionClick = () => {
    handleListItemClick(2)
    history.push('/collection')
  }

  const dispatch = useDispatch()

  const onAuthClick = () => {
    if (auth.isAuth) {
      auth.logout()
    } else {
      history.push('/login')
    }
    handleListItemClick(-1)
  }

  const renderPlaylists = playlists.map((el, idx) => {
    return (
      <MenuItem
        key={el.id}
        isSelected={selectedIndex === idx + 100}
        isOpen={isOpen}
        text={el.title}
        onClick={() => {
          setSelectedIndex(idx + 100)
          onPlaylistClick(el.id, el.dynamic, el.external_id, el.source)
        }}
      >
        <SmartImage
          src={el.thumbnail}
          alt={el.title}
          height={35}
          width={35}
          style={{
            objectFit: 'cover',
            borderRadius: '50%',
            border: '1px solid #b3b3b3',
            marginLeft: '-5px',
          }}
        />
      </MenuItem>
    )
  })

  const room = useSelector(getRoom)
  const name = useSelector(getName)

  const [tempRoom, setTempRoom] = useState(room)

  const changeRoom = (event: ChangeEvent<HTMLInputElement>) => {
    setTempRoom(parseInt(event.target.value))
  }

  const saveRoom = () => {
    dispatch(setRoom(tempRoom))
  }

  const resetRoom = () => {
    dispatch(setRoom(name))
  }

  return (
    <Drawer
      variant={'permanent'}
      classes={{
        paper: styles.Menu,
      }}
    >
      <List>
        <MenuItem
          isOpen={isOpen}
          isSelected={selectedIndex === 0}
          onClick={onHomeClick}
          text={'Главная'}
        >
          <HomeRoundedIcon />
        </MenuItem>
        <MenuItem
          isOpen={isOpen}
          isSelected={selectedIndex === -1}
          onClick={onAuthClick}
          text={auth.isAuth ? '*Выйти*' : '*Авторизация*'}
        >
          <HomeRoundedIcon />
        </MenuItem>
        <MenuItem
          isOpen={isOpen}
          isSelected={selectedIndex === 1}
          onClick={onSearchClick}
          text={'Поиск'}
        >
          <SearchRoundedIcon />
        </MenuItem>
        <MenuItem
          isOpen={isOpen}
          isSelected={selectedIndex === 2}
          onClick={onMediaCollectionClick}
          text={'Моя медиатека'}
        >
          <QueueMusicRoundedIcon />
        </MenuItem>
      </List>
      <List
        style={{ marginTop: '24px' }}
        subheader={
          <ListSubheader
            className={styles.SubHeader}
            classes={{ root: 'ellipsis-one-line' }}
          >
            ПЛЕЙЛИСТЫ
          </ListSubheader>
        }
      >
        <MenuItem
          isSelected={false}
          isOpen={isOpen}
          onClick={() => {}}
          text={'Создать плейлист'}
        >
          <AddBoxRoundedIcon />
        </MenuItem>
        <MenuItem
          isSelected={selectedIndex === 3}
          isOpen={isOpen}
          text={'Любимые треки'}
          onClick={() => {
            setSelectedIndex(3)
            onStaticPlaylistClick('0')
          }}
        >
          <FavoriteRoundedIcon />
        </MenuItem>
        <MenuItem
          isSelected={selectedIndex === 4}
          isOpen={isOpen}
          text={'Проигрываемые'}
          onClick={() => {
            setSelectedIndex(4)
            onNowPlayingClick()
          }}
        >
          <VolumeUpRoundedIcon />
        </MenuItem>
        <MenuItem
          isSelected={selectedIndex === 5}
          isOpen={isOpen}
          text={'Недавнее'}
          onClick={() => {
            setSelectedIndex(5)
          }}
        >
          <HistoryRoundedIcon />
        </MenuItem>
        <Divider />
        <>{renderPlaylists}</>
        {isOpen && (
          <>
            <Input
              value={tempRoom}
              type="number"
              onChange={changeRoom}
              placeholder="room"
            />
            <Button onClick={saveRoom}>Save room</Button>
            <Button onClick={resetRoom}>Reset room</Button>
          </>
        )}
      </List>
    </Drawer>
  )
})

export default SideMenu
