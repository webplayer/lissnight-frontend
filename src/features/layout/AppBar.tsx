import styles from './AppBar.module.scss'

import React from 'react'
import { Layout } from 'antd'
import HistoryButtons from '../router/HistoryButtons'
import SearchInput from '../search/searchInput/SearchInput'

const AppBar = () => {
  return (
    <Layout.Header className={styles.AppBar}>
      <HistoryButtons />
      <SearchInput />
    </Layout.Header>
  )
}

export default AppBar
