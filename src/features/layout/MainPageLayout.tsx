import React from 'react'
import { Layout } from 'antd'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import LeftSidebar from './LeftSidebar'
import AppBar from './AppBar'
import Content from './Content'
import RightSidebar from './RightSidebar'
import Footer from './Footer'
import styles from './MainPageLayout.module.scss'

export type ScreenWidth = 'xs' | 'sm' | 'md' | 'lg' | 'xl'

type MainPageLayoutProps = {
  width: ScreenWidth
}

const MainPageLayout: React.FC<MainPageLayoutProps> = ({ width }) => {
  return (
    <Layout className={styles.Container}>
      <Layout>
        {isWidthUp('sm', width) && <LeftSidebar />}
        <Layout className={styles.FillHeight}>
          <AppBar />
          <Content />
        </Layout>
        {isWidthUp('md', width) && <RightSidebar />}
      </Layout>
      <Footer />
    </Layout>
  )
}

export default withWidth()(MainPageLayout)
