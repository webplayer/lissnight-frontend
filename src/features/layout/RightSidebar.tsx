import React, { useState } from 'react'
import Sidebar from './Sidebar'
import { useSelector } from 'react-redux'
import ActivePlaylistSide from '../player/nowPlaying/ActivePlaylistSide'
import { getIsPlaylistEmpty } from '../../utils/Selectors'

const RightSidebar = () => {
  const [isOpen, setIsOpen] = useState(false)
  const isPlaylistEmpty = useSelector(getIsPlaylistEmpty)
  return (
    <>
      {!isPlaylistEmpty && (
        <Sidebar isOpen={isOpen} setIsOpen={setIsOpen} variant={'right'}>
          <ActivePlaylistSide />
        </Sidebar>
      )}
    </>
  )
}

export default RightSidebar
