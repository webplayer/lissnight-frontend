import styles from './Content.module.scss'
import React from 'react'
import { Layout } from 'antd'
import ContentSwitcher from '../router/ContentSwitcher'

const Content = () => {
  return (
    <Layout.Content className={styles.Content} id={'ContentWrapper'}>
      <ContentSwitcher />
    </Layout.Content>
  )
}

export default Content
