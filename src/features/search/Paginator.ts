import { AppDispatch } from '../../app/store'

class Paginator {
  static vkPageSize = 10
  static youtubePageSize = 10
  static preloadPagesCount = 1

  static getPages = (array: any[], count: number, dispatch: AppDispatch) => {}

  getPagesCount = (array: any[], source: string) => {
    switch (source) {
      case 'youtube': {
        return Math.ceil(array.length / Paginator.youtubePageSize)
      }
      case 'vk': {
        return Math.ceil(array.length / Paginator.vkPageSize)
      }
    }
    return 0
  }

  static getNextPage = () => {}
}

export default Paginator
