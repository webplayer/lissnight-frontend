import React, { useCallback, useEffect } from 'react'

import { useHistory, useLocation, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { search, searchNext } from './searchSlice'
import { Paper } from '@material-ui/core'
import { setAudio, setPlaylist } from '../player/playerSlice'
import { Audio, Playlist } from '../player/types'
import { Playlist as PlaylistT } from './types'
import { SmartImage } from '../../components/LazyImage'
import {
  getCurrentAudio,
  getCurrentPlaylistId,
  getIsSearchFetching,
  getSearchNextUrl,
  getSearchResult,
  getSearchResultInput,
} from '../../utils/Selectors'
import Helpers from '../../utils/Helpers'
import styles from './SearchContent.module.scss'

const SearchContent = () => {
  const { q } = useParams<{ q: string }>()
  const history = useHistory()
  const dispatch = useDispatch()
  const searchResultInput = useSelector(getSearchResultInput)

  useEffect(() => {
    if (q) {
      if (decodeURIComponent(q) !== searchResultInput)
        dispatch(search(decodeURIComponent(q)))
    }
  }, [q, searchResultInput])

  const searchResult = useSelector(getSearchResult)

  const currentPlaylistId = useSelector(getCurrentPlaylistId)

  const onAudioClick = (audio: Audio) => {
    if (currentPlaylistId !== `/search/${q}`) {
      const newPlaylist: Playlist = {
        id: `/search/${q}`,
        url: `/search/${q}`,
        name: `Поиск: ${decodeURIComponent(q)}`,
        audios: searchResult.searchArr.filter(
          (el) => el.type === 'audio'
        ) as Audio[],
        isDynamic: true,
      }
      dispatch(setPlaylist({ playlist: newPlaylist, audio }))
    } else {
      dispatch(setAudio(audio as Audio))
    }
  }

  const onPlaylistClick = (playlist: PlaylistT) => {
    history.push(`/playlist/dynamic/youtube/${playlist.id}`)
  }

  let location = useLocation()
  const nextPageUrl = useSelector(getSearchNextUrl)

  const isFetching = useSelector(getIsSearchFetching)

  useEffect(() => {
    let timer: number | undefined

    const el = document.getElementById('ContentWrapper')
    if (el) {
      el.onscroll = (event) => {
        if (timer) {
          window.clearTimeout(timer)
        }

        timer = window.setTimeout(function () {
          // Preload music
          const spacePreload = 50

          const toBottom = el.scrollHeight - el.clientHeight - el.scrollTop
          // console.log(toBottom)
          if (toBottom < spacePreload) {
            if (nextPageUrl && !isFetching) {
              dispatch(searchNext(nextPageUrl))
            }
          }
        }, 100)
      }
    }
    return () => {
      if (el) {
        el.onscroll = null
      }
    }
  }, [nextPageUrl, isFetching, location.pathname])

  const currentAudio = useSelector(getCurrentAudio)
  useEffect(() => {
    if (currentPlaylistId === '/search/' + searchResult.searchInput) {
      const newPlaylist: Playlist = {
        id: `/search/${q}`,
        url: `/search/${q}`,
        name: `Поиск: ${decodeURIComponent(q)}`,
        audios: searchResult.searchArr.filter(
          (el) => el.type === 'audio'
        ) as Audio[],
        isDynamic: true,
      }
      dispatch(setPlaylist({ playlist: newPlaylist, audio: currentAudio }))
    }
  }, [searchResult, currentPlaylistId, currentAudio])

  return (
    <div>
      {searchResult.searchArr.map((el) => {
        switch (el.type) {
          case 'audio': {
            return <AudioResult key={el.id} audio={el} onClick={onAudioClick} />
          }
          case 'playlist': {
            return (
              <PlaylistResult
                key={el.id}
                playlist={el}
                onClick={onPlaylistClick}
              />
            )
          }
          case 'channel': {
            return <></>
          }
          default: {
            return <></>
          }
        }
      })}
    </div>
  )
}

type AudioResultProps = {
  audio: Audio
  onClick: (audio: Audio) => void
}
const AudioResult = ({ audio, onClick }: AudioResultProps) => {
  const currentAudio = useSelector(getCurrentAudio)

  const isActive = audio.id === currentAudio.id

  const history = useHistory()
  const onArtistClick = useCallback(
    (event) => {
      event.stopPropagation()
      history.push(`/search/${audio.artist}`)
    },
    [audio.artist, history]
  )

  return (
    <Paper
      className={styles.Audio}
      style={{
        background: isActive
          ? 'linear-gradient(40deg,rgb(243 77 79 / 60%),rgb(255 152 0 / 60%))'
          : '',
      }}
      key={audio.id}
      onClick={() => {
        onClick(audio)
      }}
    >
      <img
        style={{ objectFit: 'cover' }}
        src={audio.thumbnail}
        width={100}
        height={64}
        alt={audio.name}
      />
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          overflow: 'hidden',
          paddingRight: '5px',
          width: '100%',
        }}
      >
        <div
          style={{
            paddingLeft: '5px',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            width: '100%',
          }}
        >
          {audio.name}
        </div>
        <div
          style={{
            paddingLeft: '5px',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            width: '100%',
            fontSize: '12px',
            lineHeight: '16px',
            letterSpacing: '0.015em',
            color: '#d9d8d8',
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <span onClick={onArtistClick} className={styles.Artist}>
            {audio.artist}
          </span>
          <span>{Helpers.formatTime(audio.maxTime)}</span>
        </div>
      </div>
    </Paper>
  )
}

type PlaylistResultProps = {
  playlist: PlaylistT
  onClick: (playlist: PlaylistT) => void
}
const PlaylistResult = ({ playlist, onClick }: PlaylistResultProps) => {
  return (
    <Paper
      style={{
        textAlign: 'left',
        margin: '5px',
        display: 'flex',
        alignItems: 'center',
        maxWidth: '1955px',
        overflow: 'hidden',
      }}
      key={playlist.id}
      onClick={() => {
        onClick(playlist)
      }}
    >
      <SmartImage
        style={{ objectFit: 'cover' }}
        src={playlist.thumbnail}
        width={100}
        height={64}
        alt={playlist.name}
      />
      <div
        style={{
          display: 'flex',
          flexWrap: 'wrap',
          overflow: 'hidden',
        }}
      >
        <div
          style={{
            paddingLeft: '5px',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            width: '100%',
          }}
        >
          {playlist.name}
        </div>
        <div
          style={{
            paddingLeft: '5px',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            width: '100%',
            fontSize: '12px',
            lineHeight: '16px',
            letterSpacing: '0.015em',
            color: '#d9d8d8',
          }}
        >
          {playlist.size} Аудио
        </div>
      </div>
    </Paper>
  )
}

export default SearchContent
