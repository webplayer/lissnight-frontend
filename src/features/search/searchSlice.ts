import { Search, SearchResult } from './types'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import SearchApi from '../../api/SearchApi'
import { AppDispatch, AppThunk } from '../../app/store'
import Helpers from '../../utils/Helpers'

const initialState: Search = {
  autocompleteList: [],
  isAutocompleteLoading: false,
  searchResult: { searchInput: '', searchArr: [], nextPageUrl: null },
  isFetching: false,
}

const searchSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    searchRequest(state, action: PayloadAction<string>) {
      state.searchResult.searchInput = action.payload
    },
    searchSuccess(state, action: PayloadAction<SearchResult>) {
      state.searchResult = action.payload
    },
    setAutocompleteList(state, action: PayloadAction<string[]>) {
      state.autocompleteList = action.payload
      state.isAutocompleteLoading = false
    },
    setIsAutocompleteLoading(state, action: PayloadAction<boolean>) {
      state.isAutocompleteLoading = action.payload
    },
    searchNextRequest(state, action: PayloadAction<string>) {
      state.isFetching = true
    },
    searchNextSuccess(state, action: PayloadAction<SearchResult>) {
      state.isFetching = false
      state.searchResult.nextPageUrl = action.payload.nextPageUrl

      let united = [
        ...state.searchResult.searchArr,
        ...action.payload.searchArr,
      ]

      state.searchResult.searchArr = Helpers.toUniqueArray(united)
    },
  },
})

export const search = (q: string): AppThunk => async (
  dispatch: AppDispatch
) => {
  dispatch(searchSlice.actions.searchRequest(q))
  SearchApi.search(q, 'youtube').then((res) => {
    dispatch(
      searchSlice.actions.searchSuccess({
        searchInput: q,
        searchArr: res.data,
        nextPageUrl: res.nextSearchPage,
      })
    )
  })
}

export const searchNext = (nextUrl: string): AppThunk => (
  dispatch: AppDispatch
) => {
  dispatch(searchSlice.actions.searchNextRequest(nextUrl))
  SearchApi.searchNext(nextUrl).then((res) => {
    dispatch(
      searchSlice.actions.searchNextSuccess({
        searchInput: '',
        searchArr: res.data,
        nextPageUrl: res.nextSearchPage,
      })
    )
  })
}

export const autocomplete = (q: string): AppThunk => async (
  dispatch: AppDispatch
) => {
  dispatch(searchSlice.actions.setIsAutocompleteLoading(true))
  SearchApi.autocomplete(q).then((res) => {
    dispatch(searchSlice.actions.setAutocompleteList(res))
  })
}

// export const {} = searchSlice.actions

export default searchSlice.reducer
