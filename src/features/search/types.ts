import { Audio } from '../player/types'

export type Search = {
  autocompleteList: string[]
  isAutocompleteLoading: boolean
  searchResult: SearchResult
  isFetching: boolean
}

export type Playlist = {
  type: 'playlist'
  id: string
  size: number
  name: string
  url: string
  thumbnail: string
}

export type Channel = {
  type: 'channel'
  id: string
  size: number
  name: string
  url: string
  thumbnail: string
}

export type SearchEl = Playlist | Channel | Audio

export type SearchResult = {
  searchInput: string
  searchArr: SearchEl[]
  nextPageUrl: null | string
}
