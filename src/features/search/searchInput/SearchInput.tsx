import './SearchInput.scss'
import React, { useEffect, useState } from 'react'
import { TextField } from '@material-ui/core'
import Autocomplete from '@material-ui/lab/Autocomplete'
import parse from 'autosuggest-highlight/parse'
import match from 'autosuggest-highlight/match'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { autocomplete } from '../searchSlice'
import {
  getAutocompleteList,
  getSearchResultInput,
} from '../../../utils/Selectors'

const SearchInput = () => {
  const dispatch = useDispatch()
  const history = useHistory()

  const searchInput = useSelector(getSearchResultInput)
  const [inputValue, setInputValue] = useState<string>(searchInput)
  useEffect(() => {
    setInputValue(searchInput)
  }, [searchInput, setInputValue])

  const autocompleteList = useSelector(getAutocompleteList)

  const onInputChange = (event: object, value: string) => {
    setInputValue(value)
    dispatch(autocomplete(value))
  }

  const onSearch = (event: any, newValue: string | null) => {
    if (newValue) {
      history.push(`/search/${encodeURIComponent(newValue)}`)
    }
  }

  return (
    <Autocomplete
      id="free-solo-demo"
      freeSolo
      blurOnSelect
      debug={false}
      size="small"
      loading={false}
      loadingText={'Поиск...'}
      noOptionsText={'ничего не найдено =('}
      options={autocompleteList}
      inputValue={inputValue}
      onChange={onSearch}
      onInputChange={onInputChange}
      renderInput={(params) => (
        <TextField
          {...params}
          label="Тык тык..."
          margin="normal"
          variant="outlined"
        />
      )}
      renderOption={(option, { inputValue }) => {
        const matches = match(option, inputValue)
        const parts = parse(option, matches)
        return (
          <div>
            {parts.map((part, index) => (
              <span
                key={index}
                style={{ fontWeight: !part.highlight ? 700 : 400 }}
              >
                {part.text}
              </span>
            ))}
          </div>
        )
      }}
    />
  )
}

export default SearchInput
