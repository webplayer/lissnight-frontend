import React from 'react'
import { Route, Switch } from 'react-router-dom'
import MainPageLayout from '../layout/MainPageLayout'
import PlayerFullScreenLayout from '../player/playerFullScreen/PlayerFullScreenLayout'
import RegisterPage from '../auth/components/RegisterPage'
import PlayerShare from '../player/share/PlayerShare'

const LayoutSwitcher = () => {
  return (
    <Switch>
      <Route path="/player/share/:audio">
        <PlayerShare />
      </Route>
      <Route path="/player">
        <PlayerFullScreenLayout />
      </Route>
      <Route path="/register">
        <RegisterPage />
      </Route>
      <Route path="/">
        <MainPageLayout />
      </Route>
    </Switch>
  )
}

export default LayoutSwitcher
