import React from 'react'
import { Route, Switch } from 'react-router-dom'
import SearchContent from '../search/SearchContent'
import MediaLibraryContent from '../mediaLibrary/MediaLibraryContent'
import Ambilight from '../player/videoPlayer/Ambilight'
import NowPlayingPlaylist from '../player/nowPlaying/NowPlayingPlaylist'
import StaticPlaylist from '../mediaLibrary/StaticPlaylist'
import DynamicPlaylist from '../mediaLibrary/DynamicPlaylist'

const ContentSwitcher = () => {
  return (
    <Switch>
      <Route path="/collection">
        <MediaLibraryContent />
      </Route>
      <Route path="/playlist/dynamic/:source/:id">
        <DynamicPlaylist />
      </Route>
      <Route path="/playlist/:id">
        <StaticPlaylist />
      </Route>
      <Route path="/search/:q">
        <SearchContent />
      </Route>
      <Route path="/search">
        <SearchContent />
      </Route>
      <Route path="/now-playing">
        <NowPlayingPlaylist />
      </Route>
      <Route path="/ambilight">
        <Ambilight />
      </Route>
      <Route path="/">
        <NowPlayingPlaylist />
      </Route>
    </Switch>
  )
}

export default ContentSwitcher
