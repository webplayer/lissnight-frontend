import { Audio } from '../types'
import { RefObject, useEffect, useState } from 'react'
import useVolume from './useVolume'
import useMediaSession from './useMediaSession'
import useYoutubeSkipper from './useYoutubeSkipper'

function usePlayer(
  playerRef: RefObject<HTMLAudioElement>,
  currentAudio: Audio,
  isPlaying: boolean,
  isLoop: boolean,
  currentTime: number,
  setCurrentTime: (time: number) => void,
  volume: number,
  setVolume: (volume: number) => void,
  play: () => void,
  pause: () => void,
  next: () => void,
  prev: () => void
) {
  const [time, setTime] = useState(currentTime)
  useYoutubeSkipper(currentAudio.id, time, setCurrentTime)

  const updateTime = () => {
    if (playerRef.current) {
      setTime(playerRef.current.currentTime)
    }
  }
  useEffect(() => {
    if (playerRef.current) {
      playerRef.current.addEventListener('timeupdate', updateTime)
    }
  }, [playerRef])

  useEffect(() => {}, [playerRef, time, currentAudio.url])

  useEffect(() => {
    if (playerRef.current) {
      playerRef.current.currentTime = currentTime
    }
  }, [currentTime])

  const smoothVolume = useVolume(volume, setVolume)
  useEffect(() => {
    if (playerRef.current) {
      playerRef.current.volume = smoothVolume
    }
  }, [smoothVolume])

  useMediaSession(prev, next, play, pause, currentAudio)

  useEffect(() => {
    if (playerRef.current) {
      playerRef.current.onended = next
      playerRef.current.onerror = next
    }
  }, [playerRef, next])

  useEffect(() => {
    if (playerRef.current && currentAudio && currentAudio.url !== '') {
      isPlaying ? playerRef.current.play() : playerRef.current.pause()
    }
  }, [isPlaying, currentAudio])

  return { isLoop, url: currentAudio.url }
}

export default usePlayer
