import { io, Socket } from 'socket.io-client'
import { useEffect, useState } from 'react'
import { Audio } from '../types'

const SERVER_URL = 'https://sync.puvel.ru/'

enum Command {
  Play,
  Pause,
  Next,
  Prev,
  SetAudio,
  SetTime,
}

type SyncData = {
  command: Command
  json: any
}

type SyncEvent = {
  who: number
  data: SyncData
}

function useSocketControls(
  play: () => void,
  pause: () => void,
  setTime: (time: number) => void,
  setAudio: (audio: Audio) => void,
  roomId: number,
  name: number
) {
  const [socket, setSocket] = useState<Socket>()

  useEffect(() => {
    setSocket(io(SERVER_URL, { query: { roomId: roomId.toString() } }))
  }, [roomId])

  function playSignal() {
    if (socket) {
      socket.emit('sync', { who: name, data: { command: Command.Play } })
    }
  }

  function pauseSignal() {
    if (socket) {
      socket.emit('sync', { who: name, data: { command: Command.Pause } })
    }
  }

  function setAudioSignal(audio: Audio) {
    if (socket) {
      socket.emit('sync', {
        who: name,
        data: { command: Command.SetAudio, json: audio },
      })
    }
  }

  function setTimeSignal(time: number) {
    if (socket) {
      socket.emit('sync', {
        who: name,
        data: { command: Command.SetTime, json: time },
      })
    }
  }

  useEffect(() => {
    if (socket) {
      socket.on('sync', ({ who, data }: SyncEvent) => {
        if (name !== who && roomId !== name) {
          switch (data.command) {
            case Command.Play:
              play()
              break
            case Command.Pause:
              pause()
              break
            case Command.SetAudio:
              setAudio(data.json)
              break
            case Command.SetTime:
              setTime(data.json)
              break
            default:
              break
          }
        }
      })
    }
  }, [socket, roomId, name])

  return {
    playSignal,
    pauseSignal,
    setAudioSignal,
    setTimeSignal,
  }
}

export default useSocketControls
