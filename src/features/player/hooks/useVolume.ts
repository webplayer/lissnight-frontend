import { useEffect } from 'react'

function useVolume(volume: number, setVolume: (volume: number) => void) {
  useEffect(() => {
    const localVolume = localStorage.getItem('volume')
    if (!localVolume) {
      localStorage.setItem('volume', String(volume))
    } else {
      setVolume(+localVolume)
    }
  }, [])

  return (100 ** (Number(volume) / 100) - 1) / 100
}

export default useVolume
