import SponsorBlockApi from '../../../api/SponsorBlockApi'
import { useEffect, useState } from 'react'

type Segment = {
  segment: number[]
  UUID: string
  category: string
  videoDuration: number
}

function useYoutubeSkipper(
  videoId: string,
  currentTime: number,
  setTime: (time: number) => void
) {
  const [segments, setSegments] = useState<Segment[]>([])

  useEffect(() => {
    if (videoId) {
      setSegments([])

      SponsorBlockApi.getSkipBlocks(videoId)
        .then((res) => {
          setSegments(res.data)
        })
        .catch(() => {})
    }
  }, [videoId])

  useEffect(() => {
    const skipSegment = segments.find(
      (el) => el.segment[0] <= currentTime && currentTime < el.segment[1]
    )
    if (skipSegment) {
      setTime(skipSegment.segment[1])
    }
  }, [currentTime])
}

export default useYoutubeSkipper
