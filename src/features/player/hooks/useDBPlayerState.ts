import { useIndexedDB } from 'react-indexed-db'
import { RefObject, useEffect } from 'react'
import { Player } from '../types'

function useDBPlayerState(
  playerRef: RefObject<HTMLAudioElement>,
  playerState: Player,
  setPlayerState: (state: Player) => void
) {
  const db = useIndexedDB('users')

  useEffect(() => {
    db.getByIndex('name', 'default')
      .then((res) => {
        if (res && res.playerState) {
          setPlayerState(res.playerState)
        }
      })
      .catch(() => {
        db.add({ name: 'default', playerState: playerState })
          .then(null)
          .catch(null)
        setPlayerState(playerState)
      })
  }, [])

  useEffect(() => {
    window.onbeforeunload = () => {
      if (playerRef.current) {
        db.update({
          id: 1,
          name: 'default',
          playerState: {
            ...playerState,
            isPlaying: false,
            currentTime: playerRef.current.currentTime,
          },
        })
          .then(null)
          .catch(null)
      } else {
        db.update({
          id: 1,
          name: 'default',
          playerState: { ...playerState, isPlaying: false, currentTime: 0 },
        })
          .then(null)
          .catch(null)
      }
    }
  }, [playerState])

  useEffect(() => {
    if (playerState.currentAudio) {
      if (
        playerRef.current &&
        playerState.currentAudio &&
        playerState.currentAudio.url !== ''
      ) {
        playerState.isPlaying
          ? playerRef.current.play()
          : playerRef.current.pause()
        db.update({
          id: 1,
          name: 'default',
          playerState: {
            ...playerState,
            isPlaying: false,
            currentTime: 0,
          },
        }).then()
      }
    }
  }, [playerState.currentAudio, playerState.isPlaying, playerRef])
}

export default useDBPlayerState
