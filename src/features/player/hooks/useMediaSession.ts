import { useEffect } from 'react'
import { Audio } from '../types'

function useMediaSession(
  onPrev: () => void,
  onNext: () => void,
  onPlay: () => void,
  onPause: () => void,
  currentAudio: Audio
) {
  useEffect(() => {
    if (navigator.mediaSession) {
      navigator.mediaSession.setActionHandler('previoustrack', onPrev)
      navigator.mediaSession.setActionHandler('nexttrack', onNext)
      navigator.mediaSession.setActionHandler('play', onPlay)
      navigator.mediaSession.setActionHandler('pause', onPause)
    }
  }, [onPrev, onNext, onPlay, onPause])

  useEffect(() => {
    if (navigator.mediaSession) {
      navigator.mediaSession.metadata = new MediaMetadata({
        title: currentAudio.name,
        artist: currentAudio.artist,
        artwork: [
          {
            src: currentAudio.thumbnail,
            sizes: '256x256',
            type: 'image/png',
          },
        ],
      })
    }
  }, [currentAudio])
}

export default useMediaSession
