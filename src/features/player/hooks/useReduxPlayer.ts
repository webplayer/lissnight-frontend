import { useDispatch, useSelector } from 'react-redux'
import {
  getCurrentAudio,
  getCurrentTime,
  getIsLoop,
  getIsPlaying,
  getName,
  getPlayerState,
  getRoom,
  getVolume,
} from '../../../utils/Selectors'
import useDBPlayerState from './useDBPlayerState'
import {
  playNext,
  playPrev,
  setAudio,
  setCurrentTime,
  setPlayerState,
  setPlaying,
  setVolume,
} from '../playerSlice'
import { RefObject, useEffect } from 'react'
import { Audio, Player } from '../types'
import usePlayer from './usePlayer'
import useSocketControls from './useSocketControls'

function useReduxPlayer(playerRef: RefObject<HTMLAudioElement>) {
  const dispatch = useDispatch()

  const currentAudio = useSelector(getCurrentAudio)
  const isPlaying = useSelector(getIsPlaying)
  const isLoop = useSelector(getIsLoop)
  const currentTime = useSelector(getCurrentTime)

  const playerState = useSelector(getPlayerState)
  function setReduxPlayerState(playerState: Player) {
    dispatch(setPlayerState(playerState))
  }
  useDBPlayerState(playerRef, playerState, setReduxPlayerState)

  const volume = useSelector(getVolume)
  function setReduxVolume(volume: number) {
    dispatch(setVolume(volume))
  }

  function setTime(time: number) {
    dispatch(setCurrentTime(time))
  }

  const name = useSelector(getName)
  const room = useSelector(getRoom)

  const {
    playSignal,
    pauseSignal,
    setAudioSignal,
    setTimeSignal,
  } = useSocketControls(
    () => dispatch(setPlaying(true)),
    () => dispatch(setPlaying(false)),
    setTime,
    (audio: Audio) => dispatch(setAudio(audio)),
    room,
    name
  )

  useEffect(() => {
    if (name === room) {
      if (isPlaying) {
        playSignal()
      } else {
        pauseSignal()
      }
    }
  }, [isPlaying])

  useEffect(() => {
    if (name === room) {
      setTimeSignal(currentTime)
    }
  }, [currentTime])

  useEffect(() => {
    if (name === room) {
      setAudioSignal(currentAudio)
    }
  }, [currentAudio])

  return usePlayer(
    playerRef,
    currentAudio,
    isPlaying,
    isLoop,
    currentTime,
    setTime,
    volume,
    setReduxVolume,
    () => {
      dispatch(setPlaying(true))
      playSignal()
    },
    () => {
      dispatch(setPlaying(false))
      pauseSignal()
    },
    () => {
      dispatch(playNext())
    },
    () => {
      dispatch(playPrev())
    }
  )
}

export default useReduxPlayer
