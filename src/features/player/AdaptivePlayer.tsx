import React from 'react'
import { isWidthUp, withWidth } from '@material-ui/core'
import { ScreenWidth } from '../layout/MainPageLayout'
import DesktopPlayer from './desktopPlayer/DesktopPlayer'
import MobilePlayer from './mobilePlayer/MobilePlayer'
import { useSelector } from 'react-redux'
import PlayerOverlay from './PlayerOverlay'
import { getIsPlaylistEmpty } from '../../utils/Selectors'

type AdaptivePlayerProps = {
  width: ScreenWidth
}

const AdaptivePlayer: React.FC<AdaptivePlayerProps> = ({ width }) => {
  const isPlaylistEmpty = useSelector(getIsPlaylistEmpty)
  return (
    <>
      <PlayerOverlay />
      {!isPlaylistEmpty &&
        (isWidthUp('sm', width) ? <DesktopPlayer /> : <MobilePlayer />)}
    </>
  )
}

export default withWidth()(AdaptivePlayer)
