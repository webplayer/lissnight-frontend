import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setAudio } from './playerSlice'
import { Audio } from './types'
import { RootState } from '../../app/reducer'
import MediaCell from '../../components/MediaCell'
import {
  getCurrentPlaylistId,
  getIsTargetAudioPlaying,
} from '../../utils/Selectors'

type AudioCellProps = {
  audio: Audio
  playlistId?: string
  onPlaylistSet?: (audio: Audio) => void
}
const AudioCell: React.FC<AudioCellProps> = ({
  audio,
  playlistId,
  onPlaylistSet,
}) => {
  const dispatch = useDispatch()

  const currentPlaylistId = useSelector(getCurrentPlaylistId)

  const onButtonClick = () => {
    if (onPlaylistSet && playlistId && playlistId !== currentPlaylistId) {
      onPlaylistSet(audio)
    } else {
      dispatch(setAudio(audio))
    }
  }

  const isPlaying = useSelector((state: RootState) =>
    getIsTargetAudioPlaying(state, audio.id)
  )

  return (
    <MediaCell
      isPlaying={isPlaying}
      thumbnail={audio.thumbnail}
      onButtonClick={onButtonClick}
      onCellClick={onButtonClick}
      info={audio.name}
      artist={audio.artist}
      time={audio.maxTime}
    />
  )
}

export default AudioCell
