import React, { useEffect, useState } from 'react'
import { Grid, Slider } from '@material-ui/core'
import ShuffleButton from '../buttons/ShuffleButton'
import PrevButton from '../buttons/PrevButton'
import PlayButton from '../buttons/PlayButton'
import NextButton from '../buttons/NextButton'
import LoopButton from '../buttons/LoopButton'
import { useDispatch } from 'react-redux'
import { setCurrentTime as setCurrentTimeState } from '../playerSlice'
import Helpers from '../../../utils/Helpers'

const DesktopPlayerControls = () => {
  return (
    <Grid container direction="column" justify="center" alignItems="center">
      <ControlButtons />
      <TimeControl />
    </Grid>
  )
}

export const ControlButtons = () => {
  return (
    <Grid item>
      <ShuffleButton />
      <PrevButton />
      <PlayButton />
      <NextButton />
      <LoopButton />
    </Grid>
  )
}

export const TimeControl = () => {
  const [isSeek, setIsSeek] = useState<boolean>(false)
  const [seekValue, setSeekValue] = useState<number>(0)
  const handleChange = (event: any, newValue: number | number[]) => {
    setIsSeek(true)
    setSeekValue(newValue as number)
  }
  const [currentTime, setCurrentTime] = useState<number>(0)
  const [maxTime, setMaxTime] = useState(0)

  const dispatch = useDispatch()

  const getCurrentTime = (): string => {
    return Helpers.formatTime(currentTime)
  }
  const getLeftTime = (): string => {
    return Helpers.formatTime(maxTime - currentTime)
  }

  const getSeekTime = (): string => {
    return Helpers.formatTime(seekValue)
  }
  const getSeekLeftTime = (): string => {
    return Helpers.formatTime(maxTime - seekValue)
  }

  const setTime = () => {
    if (playerRef?.duration) {
      playerRef.currentTime = seekValue
      setCurrentTime(seekValue)
      dispatch(setCurrentTimeState(seekValue))
    }
    setIsSeek(false)
  }

  let [playerRef, setPlayerRef] = useState<HTMLAudioElement | null>(null)
  const updateTime = () => {
    if (playerRef) {
      setCurrentTime(playerRef.currentTime)
    }
  }
  useEffect(() => {
    const player: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement
    if (player) {
      setPlayerRef(player)
      player.addEventListener('timeupdate', updateTime)
      if (playerRef?.duration) {
        setMaxTime(playerRef.duration)
      }
      player.onloadedmetadata = () => {
        if (playerRef) {
          setMaxTime(playerRef.duration)
        }
      }
      if (playerRef?.currentTime) {
        setCurrentTime(playerRef.currentTime)
      }
    }
    return () => {
      if (player) {
        player.ontimeupdate = null
        player.onloadedmetadata = null
        player.removeEventListener('timeupdate', updateTime)
      }
    }
  }, [playerRef])

  return (
    <Grid
      item
      container
      spacing={2}
      direction="row"
      justify="center"
      alignItems="center"
    >
      <Grid item>{isSeek ? getSeekTime() : getCurrentTime()}</Grid>
      <Grid
        item
        xs
        style={{ padding: 0 }}
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Slider
          value={isSeek ? seekValue : currentTime}
          onChange={handleChange}
          onChangeCommitted={setTime}
          min={0}
          max={maxTime}
        />
      </Grid>
      <Grid item>{isSeek ? getSeekLeftTime() : getLeftTime()}</Grid>
    </Grid>
  )
}

export default DesktopPlayerControls
