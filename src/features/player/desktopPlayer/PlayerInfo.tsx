import React, { useCallback } from 'react'
import { Grid } from '@material-ui/core'
import styles from './PlayerInfo.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import LikeButton from '../buttons/LikeButton'
import { setIsOverlayOpen } from '../playerSlice'
import { useHistory } from 'react-router-dom'
import { SmartImage } from '../../../components/LazyImage'
import { getCurrentAudio } from '../../../utils/Selectors'

const PlayerInfo = () => {
  const currentAudio = useSelector(getCurrentAudio)
  const history = useHistory()
  const dispatch = useDispatch()

  const onOpenPlayerClick = () => {
    history.push(history.location.pathname, { isOpen: true })
    dispatch(setIsOverlayOpen(true))
  }

  const onArtistClick = useCallback(() => {
    history.push(`/search/${currentAudio.artist}`)
  }, [currentAudio.artist, history])

  return (
    <Grid
      className={styles.Info}
      container
      direction="row"
      justify="flex-start"
      alignItems="center"
    >
      <div onClick={onOpenPlayerClick}>
        <SmartImage
          src={currentAudio.thumbnail}
          alt={currentAudio.name}
          height={56}
          width={90}
          className={styles.Image}
        />
      </div>
      <Grid item className={styles.Meta}>
        <div className={styles.SongName}>{currentAudio.name}</div>
        <div className={styles.Artist} onClick={onArtistClick}>
          {currentAudio.artist}
        </div>
      </Grid>
      <LikeButton />
    </Grid>
  )
}

export default PlayerInfo
