import React from 'react'
import { SwipeableDrawer, withWidth } from '@material-ui/core'
import PlayerFullScreenLayout from './playerFullScreen/PlayerFullScreenLayout'
import { useDispatch, useSelector } from 'react-redux'
import { setIsOverlayOpen } from './playerSlice'
import styles from './PlayerOverlay.module.scss'
import { getIsOverlayOpen } from '../../utils/Selectors'

const PlayerOverlay = () => {
  const isOverlayOpen = useSelector(getIsOverlayOpen)
  const dispatch = useDispatch()

  const handleClose = () => {
    dispatch(setIsOverlayOpen(false))
  }

  const handleOpen = () => {
    dispatch(setIsOverlayOpen(true))
  }

  return (
    <SwipeableDrawer
      anchor={'bottom'}
      open={isOverlayOpen}
      onOpen={handleOpen}
      onClose={handleClose}
      disableBackdropTransition
      classes={{ paper: styles.Modal }}
      // swipeAreaWidth={isWidthUp('sm', width) ? 15 : 50}
      disableSwipeToOpen={true}
      // disableDiscovery={true}
    >
      <PlayerFullScreenLayout isOverlay={true} />
    </SwipeableDrawer>
  )
}

export default withWidth()(PlayerOverlay)
