import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Menu } from 'antd'
import './ActivePlaylistSide.scss'
import SideMenuWrapper from '../../../components/SideMenuWrapper'
import { SmartImage } from '../../../components/LazyImage'
import { setAudio } from '../playerSlice'
import {
  getCurrentAudio,
  getCurrentPlaylistAudios,
} from '../../../utils/Selectors'

const ActivePlaylistSide = () => {
  const audios = useSelector(getCurrentPlaylistAudios)
  const currentAudio = useSelector(getCurrentAudio)
  const dispatch = useDispatch()

  const renderAudios = audios.map((audio) => (
    <Menu.Item
      style={{ paddingLeft: 0 }}
      key={audio.id}
      icon={
        <SmartImage
          src={audio.thumbnail}
          alt={audio.name}
          height={40}
          width={40}
          style={{ objectFit: 'cover', marginRight: '5px' }}
        />
      }
      onClick={() => dispatch(setAudio(audio))}
    >
      {audio.name}-{audio.artist}
    </Menu.Item>
  ))

  return (
    <SideMenuWrapper selectedIdx={currentAudio.id} isControlled>
      {renderAudios}
    </SideMenuWrapper>
  )
}

export default ActivePlaylistSide
