import React from 'react'
import { useSelector } from 'react-redux'
import AudioCell from '../AudioCell'
import CellsGrid from '../../../components/CellsGrid'
import { getCurrentPlaylistAudios } from '../../../utils/Selectors'

const NowPlayingPlaylist = () => {
  const audios = useSelector(getCurrentPlaylistAudios)
  const renderAudios = audios.map((el) => <AudioCell key={el.id} audio={el} />)

  return <CellsGrid>{renderAudios}</CellsGrid>
}

export default NowPlayingPlaylist
