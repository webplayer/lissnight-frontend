import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import React, { useEffect, useState } from 'react'
import { setIsOverlayOpen } from '../playerSlice'
import { IconButton } from '@material-ui/core'
import KeyboardArrowDownRoundedIcon from '@material-ui/icons/KeyboardArrowDownRounded'

type ClosePlayerButtonProps = {
  isHistory?: boolean
}

const ClosePlayerButton: React.FC<ClosePlayerButtonProps> = ({
  isHistory = true,
}) => {
  const history = useHistory()

  const dispatch = useDispatch()

  const [isHistoryBack, setIsHistoryBack] = useState(true)

  useEffect(() => {
    if (!isHistory) {
      window.onpopstate = (event: PopStateEvent) => {
        event.preventDefault()
        setIsHistoryBack(true)
        dispatch(setIsOverlayOpen(false))
      }
      return () => {
        window.onpopstate = () => {}
      }
    }
  }, [isHistory, isHistoryBack, dispatch])

  const onBack = () => {
    if (isHistory) {
      if (history.length > 2) {
        history.goBack()
      } else {
        history.push('/')
      }
    } else {
      setIsHistoryBack(false)
      dispatch(setIsOverlayOpen(false))
    }
  }

  useEffect(() => {
    if (isHistoryBack) {
      return () => {
        history.goForward()
      }
    } else {
    }
  }, [isHistoryBack, history])

  return (
    <IconButton onClick={onBack} style={{ zIndex: 1000 }}>
      <KeyboardArrowDownRoundedIcon fontSize={'large'} />
    </IconButton>
  )
}

export default ClosePlayerButton
