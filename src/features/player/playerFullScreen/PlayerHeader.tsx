import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../../../app/reducer'
import { useHistory } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import { setIsOverlayOpen } from '../playerSlice'
import { Layout } from 'antd'
import { IconButton, Snackbar, Typography } from '@material-ui/core'
import FullscreenRoundedIcon from '@material-ui/icons/FullscreenRounded'
import ShareIcon from '@material-ui/icons/Share'
import { Alert } from '@material-ui/lab'
import ClosePlayerButton from './ClosePlayerButton'

type PlayerHeaderProps = {
  isOverlay: boolean
}
const PlayerHeader: React.FC<PlayerHeaderProps> = ({ isOverlay }) => {
  const playlistTitle = useSelector(
    (state: RootState) => state.player.playlist.name
  )

  function requestFullscreen(element: any) {
    if (element.requestFullscreen) {
      element
        .requestFullscreen()
        .then(() => {
          window.screen.orientation.lock('landscape').then(null)
        })
        // @ts-ignore
        .catch((err) => {
          console.log(`${err.message} (${err.name})`)
        })
    } else if (element.mozRequestFullScreen) {
      element
        .mozRequestFullScreen()
        .then(() => {
          window.screen.orientation.lock('landscape').then(null)
        })
        // @ts-ignore
        .catch((err) => {
          console.log(`${err.message} (${err.name})`)
        })
    } else if (element.webkitRequestFullScreen) {
      if (element) {
        element
          .webkitRequestFullScreen()
          .then(() => {
            window.screen.orientation.lock('landscape').then(null)
          })
          // @ts-ignore
          .catch((err) => {
            console.log(`${err.message} (${err.name})`)
          })
      }
    }
  }

  const toggleFullScreen = () => {
    let elem = document.getElementById('video')
    if (elem) {
      // @ts-ignore
      const isFullscreenEnabled =
        document.fullscreenEnabled ||
        // @ts-ignore
        document.mozFullScreenEnabled ||
        // @ts-ignore
        document.documentElement.webkitRequestFullScreen

      if (isFullscreenEnabled) {
        requestFullscreen(elem)
      } else {
        document.exitFullscreen().then(null)
      }
    }
  }

  const dispatch = useDispatch()
  const history = useHistory()

  const isOverlayOpen = useSelector(
    (state: RootState) => state.player.isOverlayOpen
  )

  useEffect(() => {
    history.location.state = { isOpen: true }
  }, [history])

  const onNowPlayingClick = () => {
    if (isOverlay) {
      dispatch(setIsOverlayOpen(false))
    }
    history.push('/now-playing')
  }

  const currentAudio = useSelector(
    (state: RootState) => state.player.currentAudio
  )
  const [isAlert, setIsAlert] = useState(false)
  const closeAlert = () => {
    setIsAlert(false)
  }
  const onMenuClick = () => {
    const shareLink = `https://lissn.ru/player/share/${encodeURIComponent(
      JSON.stringify(currentAudio)
    )}`
    shareVk(
      shareLink,
      currentAudio.name,
      currentAudio.thumbnail,
      currentAudio.artist
    )
    if (navigator && navigator.clipboard) {
      navigator.clipboard
        .writeText(shareLink)
        .then(() => {
          setIsAlert(true)
        })
        .catch((err) => {
          console.log('Something went wrong', err)
        })
    }
  }

  const shareVk = (
    purl: string,
    ptitle: string,
    pimg: string,
    text: string
  ) => {
    let shareUrl = ''
    shareUrl = 'https://vk.com/share.php?'
    shareUrl += 'url=' + encodeURIComponent(purl)
    shareUrl += '&title=' + encodeURIComponent(ptitle)
    shareUrl += '&description=' + encodeURIComponent(text)
    shareUrl += '&image=' + encodeURIComponent(pimg)
    shareUrl += '&noparse=true'
    window.open(shareUrl, '', 'toolbar=0,status=0,width=626,height=436')
  }

  return (
    <Layout.Header
      className={'dev'}
      style={{
        display: 'flex',
        padding: 0,
        justifyContent: 'space-between',
        flexWrap: 'nowrap',
        backgroundColor: 'rgb(18,18,18)',
        // zIndex: 1000,
      }}
    >
      <ClosePlayerButton isHistory={!isOverlayOpen} />
      <div
        onClick={onNowPlayingClick}
        style={{
          cursor: 'pointer',
          userSelect: 'none',
          color: 'white',
          overflow: 'hidden',
          zIndex: 1000,
        }}
      >
        <Typography
          variant={'subtitle1'}
          style={{
            color: 'white',
            fontSize: '0.75rem',
            textAlign: 'center',
            zIndex: 1000,
          }}
        >
          Плейлист
        </Typography>
        <Typography
          variant={'h6'}
          style={{
            zIndex: 1000,
            color: 'white',
            fontSize: '1rem',
            textAlign: 'center',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            overflow: 'hidden',
          }}
        >
          {playlistTitle}
        </Typography>
      </div>
      <div style={{ minWidth: '118px', zIndex: 1000 }}>
        <IconButton onClick={toggleFullScreen}>
          <FullscreenRoundedIcon fontSize={'large'} />
        </IconButton>
        <IconButton onClick={onMenuClick}>
          <ShareIcon fontSize={'large'} />
        </IconButton>
        <Snackbar
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={isAlert}
          autoHideDuration={3000}
          onClose={closeAlert}
        >
          <Alert onClose={closeAlert} severity="success">
            Ссылка сохранена в буфер обмена
          </Alert>
        </Snackbar>
      </div>
    </Layout.Header>
  )
}

export default PlayerHeader
