import React from 'react'
import { Layout } from 'antd'
import PlayerHeader from './PlayerHeader'
import PlayerContent from './PlayerContent'
import PlayerFooter from './PlayerFooter'

type PlayerFullScreenLayoutProps = {
  isOverlay?: boolean
}

const PlayerFullScreenLayout: React.FC<PlayerFullScreenLayoutProps> = ({
  isOverlay = false,
}) => {
  return (
    <Layout
      style={{
        height: '100%',
        minHeight: '-webkit-fill-available',
        maxHeight: '-webkit-fill-available',
        color: 'white',
      }}
    >
      <PlayerHeader isOverlay={isOverlay} />
      <PlayerContent />
      <PlayerFooter />
    </Layout>
  )
}

export default PlayerFullScreenLayout
