import { ScreenWidth } from '../../layout/MainPageLayout'
import { Grid, isWidthUp, withWidth } from '@material-ui/core'
import { Layout } from 'antd'
import { TimeControl } from '../desktopPlayer/DesktopPlayerControls'
import ShuffleButton from '../buttons/ShuffleButton'
import PrevButton from '../buttons/PrevButton'
import PlayButton from '../buttons/PlayButton'
import NextButton from '../buttons/NextButton'
import LoopButton from '../buttons/LoopButton'
import DesktopPlayerExtra from '../desktopPlayer/DesktopPlayerExtra'
import React from 'react'
import VideoButton from '../buttons/VideoButton'

type PlayerFooterProps = {
  width: ScreenWidth
}
const PlayerFooter: React.FC<PlayerFooterProps> = ({ width }) => {
  return (
    <Layout.Footer
      className={'dev'}
      style={{ padding: 0, backgroundColor: 'black' }}
    >
      <div
        className={'dev'}
        style={{
          height: isWidthUp('sm', width) ? '30px' : '60px',
          paddingLeft: '20px',
          paddingRight: '20px',
        }}
      >
        <TimeControl />
      </div>
      <div
        className={'dev'}
        style={{
          height: isWidthUp('sm', width) ? '80px' : '140px',
          flexWrap: 'nowrap',
          overflow: 'hidden',
          justifyContent: 'center',
        }}
      >
        {isWidthUp('md', width) && <Grid item xs={5} />}
        <Grid
          item
          container
          justify={'space-around'}
          alignItems={'center'}
          style={{ maxWidth: '320px' }}
        >
          <ShuffleButton size={'large'} />
          <PrevButton size={'large'} />
          <PlayButton size={'large'} />
          <NextButton size={'large'} />
          <LoopButton size={'large'} />
          <VideoButton size={'large'} />
        </Grid>
        {isWidthUp('md', width) && (
          <Grid item xs={5}>
            <DesktopPlayerExtra />
          </Grid>
        )}
      </div>
    </Layout.Footer>
  )
}

export default withWidth()(PlayerFooter)
