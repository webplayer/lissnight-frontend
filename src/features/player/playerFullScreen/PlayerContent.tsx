import { ScreenWidth } from '../../layout/MainPageLayout'
import { isWidthUp, Typography, withWidth } from '@material-ui/core'
import { Layout } from 'antd'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  getCurrentAudio,
  getIsVideo,
  getSmartVideoUrl,
} from '../../../utils/Selectors'
import { setVideo } from '../playerSlice'
import Helpers from '../../../utils/Helpers'
import Ambilight from '../videoPlayer/Ambilight'

type PlayerContentProps = {
  width: ScreenWidth
}
const PlayerContent: React.FC<PlayerContentProps> = ({ width }) => {
  const dispatch = useDispatch()
  const currentAudio = useSelector(getCurrentAudio)
  const smartVideoUrl = useSelector(getSmartVideoUrl)
  useEffect(() => {
    if (!smartVideoUrl) {
      Helpers.findVideo(
        currentAudio.name + ' ' + currentAudio.artist,
        currentAudio.maxTime
      ).then((r) => {
        if (r?.id) {
          dispatch(setVideo(r.id))
        }
      })
    }
  }, [
    smartVideoUrl,
    currentAudio.name,
    currentAudio.artist,
    currentAudio.maxTime,
    dispatch,
  ])

  const isVideo = useSelector(getIsVideo)

  return (
    <Layout.Content
      className={'dev'}
      style={{
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}
    >
      <div
        className={'dev'}
        style={{
          height: 'calc(100% - 60px)',
          backgroundColor: 'black',
        }}
      >
        {isVideo ? (
          <Ambilight />
        ) : (
          <img
            alt={currentAudio.name}
            src={currentAudio.thumbnail}
            style={{
              objectFit: 'cover',
              maxWidth: '100%',
              maxHeight: '100%',
            }}
          />
        )}
      </div>
      <div
        className={'dev'}
        style={{
          padding: '5px',
          textAlign: 'center',
          height: isWidthUp('sm', width) ? '60px' : '60px',
          backgroundColor: 'black',
          // zIndex: 1000,
        }}
      >
        <Typography variant={'h6'} color={'primary'} style={{ zIndex: 1000 }}>
          {currentAudio.name}
        </Typography>
      </div>
    </Layout.Content>
  )
}

export default withWidth()(PlayerContent)
