import React from 'react'
import { Grid } from '@material-ui/core'
import styles from './MobilePlayer.module.scss'
import PlayButton from '../buttons/PlayButton'
import MobilePlayerInfo from './MobilePlayerInfo'
import LikeButton from '../buttons/LikeButton'
import MobilePlayerControls from './MobilePlayerControls'

const MobilePlayer = () => (
  <Grid
    container
    direction="row"
    justify="space-around"
    alignItems="center"
    className={styles.Player}
  >
    <Grid
      item
      xs={9}
      sm={10}
      md={11}
      container
      direction="row"
      justify="flex-start"
      alignItems="center"
    >
      <MobilePlayerInfo />
    </Grid>
    <Grid
      item
      xs={3}
      sm={2}
      md={1}
      container
      direction="row"
      justify="flex-end"
      alignItems="center"
    >
      <MobilePlayerControls />
    </Grid>
  </Grid>
)

export default MobilePlayer
