import React, { useCallback } from 'react'
import { Grid } from '@material-ui/core'
import styles from './MobilePlayerInfo.module.scss'
import { useDispatch, useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { setIsOverlayOpen } from '../playerSlice'
import { getCurrentAudio } from '../../../utils/Selectors'
import { SmartImage } from '../../../components/LazyImage'

const MobilePlayerInfo = () => {
  const currentAudio = useSelector(getCurrentAudio)

  const dispatch = useDispatch()
  const history = useHistory()

  const onOpenPlayerClick = () => {
    history.push(history.location.pathname, { isOpen: true })
    dispatch(setIsOverlayOpen(true))
  }

  const onArtistClick = useCallback(() => {
    history.push(`/search/${currentAudio.artist}`)
  }, [currentAudio.artist, history])

  return (
    <Grid
      className={styles.Info}
      container
      direction="row"
      justify="flex-start"
      alignItems="center"
    >
      <div onClick={onOpenPlayerClick}>
        <SmartImage
          src={currentAudio.thumbnail}
          alt={currentAudio.name}
          height={48}
          width={80}
          className={styles.Image}
        />
      </div>
      <Grid item className={styles.Meta}>
        <div className={styles.SongName} onClick={onOpenPlayerClick}>
          {currentAudio.name}
        </div>
        <div className={styles.Artist} onClick={onArtistClick}>
          {currentAudio.artist}
        </div>
      </Grid>
    </Grid>
  )
}

export default MobilePlayerInfo
