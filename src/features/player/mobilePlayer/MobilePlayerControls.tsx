import { Grid } from '@material-ui/core'
import LikeButton from '../buttons/LikeButton'
import PlayButton from '../buttons/PlayButton'
import React from 'react'

const MobilePlayerControls = () => {
  return (
    <Grid
      container
      direction="row"
      justify="flex-end"
      alignItems="center"
      style={{ flexWrap: 'nowrap' }}
    >
      <Grid item>
        <LikeButton />
      </Grid>
      <Grid item>
        <PlayButton isMobile />
      </Grid>
    </Grid>
  )
}

export default MobilePlayerControls
