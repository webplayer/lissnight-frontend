import SkipNextRoundedIcon from '@material-ui/icons/SkipNextRounded'
import { IconButton } from '@material-ui/core'
import React from 'react'
import { useDispatch } from 'react-redux'
import { playNext } from '../playerSlice'
import IconButtonProps from './types'

const NextButton: React.FC<IconButtonProps> = ({
  isMobile = false,
  size = 'default',
}) => {
  const dispatch = useDispatch()
  const onNext = () => {
    dispatch(playNext())
  }

  let fontSize = '1.5rem'
  let wrapperStyle = { padding: '12px' }
  if (size === 'large') {
    fontSize = '3rem'
    wrapperStyle = { padding: '0' }
  }

  if (isMobile) {
    wrapperStyle = { padding: '3px' }
  }

  return (
    <IconButton aria-label="next audio" onClick={onNext} style={wrapperStyle}>
      <SkipNextRoundedIcon
        fontSize={isMobile ? 'large' : 'default'}
        style={{ fontSize: fontSize }}
      />
    </IconButton>
  )
}

export default NextButton
