import PlayArrowRoundedIcon from '@material-ui/icons/PlayArrowRounded'
import PauseRoundedIcon from '@material-ui/icons/PauseRounded'
import { IconButton } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setCurrentTime, setPlaying } from '../playerSlice'
import IconButtonProps from './types'
import { getIsPlaying } from '../../../utils/Selectors'

const PlayButton: React.FC<IconButtonProps> = ({ size = 'default' }) => {
  const dispatch = useDispatch()
  const isPlaying = useSelector(getIsPlaying)
  const onPlay = () => {
    dispatch(setPlaying(!isPlaying))
    if (playerRef) {
      dispatch(setCurrentTime(playerRef.currentTime))
    }
  }

  let [playerRef, setPlayerRef] = useState<HTMLAudioElement | null>(null)
  useEffect(() => {
    const player: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement
    if (player) {
      setPlayerRef(player)
    }
  }, [playerRef])

  let fontSize = '3rem'
  if (size === 'large') {
    fontSize = '5rem'
  }

  return (
    <IconButton aria-label="play/pause" style={{ padding: 0 }} onClick={onPlay}>
      {isPlaying ? (
        <PauseRoundedIcon fontSize={'large'} style={{ fontSize: fontSize }} />
      ) : (
        <PlayArrowRoundedIcon
          fontSize={'large'}
          style={{ fontSize: fontSize }}
        />
      )}
    </IconButton>
  )
}

export default PlayButton
