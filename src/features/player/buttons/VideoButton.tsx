import { IconButton } from '@material-ui/core'
import VideocamIcon from '@material-ui/icons/Videocam'
import VideocamOffIcon from '@material-ui/icons/VideocamOff'
import React from 'react'
import { RootState } from '../../../app/reducer'
import { useDispatch, useSelector } from 'react-redux'
import { setIsVideo } from '../playerSlice'
import IconButtonProps from './types'

const LoopButton: React.FC<IconButtonProps> = ({ size = 'default' }) => {
  const dispatch = useDispatch()
  const isVideo = useSelector((state: RootState) => state.player.isVideo)
  const onLoop = () => {
    dispatch(setIsVideo(!isVideo))
  }

  let fontSize = '1.5rem'
  let wrapperStyle = { padding: '12px' }
  if (size === 'large') {
    fontSize = '2rem'
    wrapperStyle = { padding: '0' }
  }

  return (
    <IconButton
      color={isVideo ? 'primary' : 'default'}
      aria-label="repeat audio"
      onClick={onLoop}
      style={wrapperStyle}
    >
      {isVideo ? (
        <VideocamIcon style={{ fontSize: fontSize }} />
      ) : (
        <VideocamOffIcon style={{ fontSize: fontSize }} />
      )}
    </IconButton>
  )
}

export default LoopButton
