import { IconButton } from '@material-ui/core'
import ShuffleOutlinedIcon from '@material-ui/icons/ShuffleOutlined'
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setShuffle } from '../playerSlice'
import IconButtonProps from './types'
import { getIsShuffle } from '../../../utils/Selectors'

const ShuffleButton: React.FC<IconButtonProps> = ({ size = 'default' }) => {
  const dispatch = useDispatch()
  const isShuffle = useSelector(getIsShuffle)
  const onShuffle = () => {
    dispatch(setShuffle(!isShuffle))
  }

  let fontSize = '1.5rem'
  let wrapperStyle = { padding: '12px' }
  if (size === 'large') {
    fontSize = '2rem'
    wrapperStyle = { padding: '0' }
  }

  return (
    <IconButton
      color={isShuffle ? 'primary' : 'default'}
      aria-label="shuffle playlist"
      onClick={onShuffle}
      style={wrapperStyle}
    >
      <ShuffleOutlinedIcon style={{ fontSize: fontSize }} />
    </IconButton>
  )
}

export default ShuffleButton
