type IconButtonProps = {
  isMobile?: boolean,
  size?: 'large' | 'small' | 'mobile' | 'default',
}

export default IconButtonProps
