import SkipPreviousRoundedIcon from '@material-ui/icons/SkipPreviousRounded'
import { IconButton } from '@material-ui/core'
import React from 'react'
import { useDispatch } from 'react-redux'
import { playPrev } from '../playerSlice'
import IconButtonProps from './types'

const PrevButton: React.FC<IconButtonProps> = ({
  isMobile = false,
  size = 'default',
}) => {
  const dispatch = useDispatch()
  const onPrev = () => {
    dispatch(playPrev())
  }

  let fontSize = '1.5rem'
  let wrapperStyle = { padding: '12px' }
  if (size === 'large') {
    fontSize = '3rem'
    wrapperStyle = { padding: '0' }
  }

  if (isMobile) {
    wrapperStyle = { padding: '3px' }
  }

  return (
    <IconButton
      aria-label="previous audio"
      onClick={onPrev}
      style={wrapperStyle}
    >
      <SkipPreviousRoundedIcon
        fontSize={isMobile ? 'large' : 'default'}
        style={{ fontSize: fontSize }}
      />
    </IconButton>
  )
}

export default PrevButton
