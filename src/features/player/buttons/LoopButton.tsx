import RepeatRoundedIcon from '@material-ui/icons/RepeatRounded'
import { IconButton } from '@material-ui/core'
import React from 'react'
import { RootState } from '../../../app/reducer'
import { useDispatch, useSelector } from 'react-redux'
import { setLoop } from '../playerSlice'
import IconButtonProps from './types'

const LoopButton: React.FC<IconButtonProps> = ({ size = 'default' }) => {
  const dispatch = useDispatch()
  const isLoop = useSelector((state: RootState) => state.player.isLoop)
  const onLoop = () => {
    dispatch(setLoop(!isLoop))
  }

  let fontSize = '1.5rem'
  let wrapperStyle = { padding: '12px' }
  if (size === 'large') {
    fontSize = '2rem'
    wrapperStyle = { padding: '0' }
  }

  return (
    <IconButton
      color={isLoop ? 'primary' : 'default'}
      aria-label="repeat audio"
      onClick={onLoop}
      style={wrapperStyle}
    >
      <RepeatRoundedIcon style={{ fontSize: fontSize }} />
    </IconButton>
  )
}

export default LoopButton
