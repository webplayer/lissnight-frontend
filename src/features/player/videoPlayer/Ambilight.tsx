import React, { useEffect, useState } from 'react'
import './Ambilight.scss'
import AudioApi, { FormatsRes } from '../../../api/AudioApi'
import { useSelector } from 'react-redux'
import { getIsPlaying, getSmartVideoUrl } from '../../../utils/Selectors'

const Ambilight = () => {
  const isPlaying = useSelector(getIsPlaying)

  const [videoUrl, setVideoUrl] = useState<string>('')

  const smartVideoUrl = useSelector(getSmartVideoUrl)

  useEffect(() => {
    if (smartVideoUrl) {
      AudioApi.getVideoFormats(smartVideoUrl).then((res: FormatsRes[]) => {
        const video = res.find((el) => el.quality.includes('720'))
        if (video) {
          setVideoUrl(video.url)
        }
      })
    }
  }, [smartVideoUrl])

  useEffect(() => {
    const player: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement
    let video: HTMLVideoElement = document.getElementById(
      'video'
    ) as HTMLVideoElement
    let timerId: NodeJS.Timeout | null = null
    timerId = setInterval(() => {
      if (video && player) {
        let diff = Math.abs(player.currentTime - video.currentTime)
        if (diff > 0.5) {
          video.currentTime = player.currentTime
        }
      }
    }, 1000)
    return () => {
      if (timerId) {
        clearInterval(timerId)
      }
    }
  }, [videoUrl])

  useEffect(() => {
    let canvas: HTMLCanvasElement = document.getElementById(
      'myCanvas'
    ) as HTMLCanvasElement
    if (canvas) {
      let ctx = canvas.getContext('2d')

      let video: HTMLVideoElement = document.getElementById(
        'video'
      ) as HTMLVideoElement

      let timerID: NodeJS.Timeout

      video.addEventListener('play', function () {
        timerID = setInterval(function () {
          if (ctx) {
            ctx.drawImage(video, 0, 0, 600, 337)
          }
        }, 30)
      })

      video.addEventListener('pause', function () {
        clearInterval(timerID)
      })

      video.addEventListener('ended', function () {
        clearInterval(timerID)
      })
    }
  }, [])

  const onPlay = () => {
    let video: HTMLVideoElement = document.getElementById(
      'video'
    ) as HTMLVideoElement
    let audio: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement
    audio.play().then(null)
  }

  const onPause = (event: any) => {
    let audio: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement
    let video: HTMLVideoElement = document.getElementById(
      'video'
    ) as HTMLVideoElement
    console.log(event)
    audio.pause()
  }

  const onPlayVideo = () => {
    let video: HTMLVideoElement = document.getElementById(
      'video'
    ) as HTMLVideoElement
    let audio: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement
    video.currentTime = audio.currentTime
    video.play().then(null)
  }

  const onPauseVideo = () => {
    let video: HTMLVideoElement = document.getElementById(
      'video'
    ) as HTMLVideoElement
    let audio: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement
    video.currentTime = audio.currentTime
    video.pause()
  }

  useEffect(() => {
    if (isPlaying) {
      onPlayVideo()
    } else {
      onPauseVideo()
    }
  }, [isPlaying])

  return (
    <div className="player">
      <video
        autoPlay={true}
        id="video"
        src={videoUrl}
        // onPlay={onPlay}
        // onPause={onPause}
      />
      <canvas id="myCanvas" width="600" height="337" />
    </div>
  )
}

export default Ambilight
