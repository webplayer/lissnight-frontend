import React, { useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { setIsVideo, setPlaying } from '../playerSlice'
import ReactPlayer from 'react-player'
import { getIsPlaying, getSmartVideoUrl } from '../../../utils/Selectors'

const VideoPlayer = () => {
  const videoPlayerRef = useRef<ReactPlayer>(null)

  const dispatch = useDispatch()

  useEffect(() => {
    if (videoPlayerRef.current) {
      dispatch(setIsVideo(true))
    }
  }, [videoPlayerRef, dispatch])

  useEffect(() => {
    const player: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement
    let timerId: NodeJS.Timeout | null = null
    timerId = setInterval(() => {
      if (videoPlayerRef.current && player) {
        let diff = Math.abs(
          player.currentTime - videoPlayerRef.current.getCurrentTime()
        )
        if (diff > 0.5) {
          videoPlayerRef.current.seekTo(player.currentTime + 0.5, 'seconds')
        }
      }
    }, 1000)
    return () => {
      if (timerId) {
        clearInterval(timerId)
      }
    }
  }, [videoPlayerRef])

  const videoUrl = useSelector(getSmartVideoUrl)

  const isPlaying = useSelector(getIsPlaying)

  const onPause = () => {
    dispatch(setPlaying(false))
  }
  const onPlay = () => {
    dispatch(setPlaying(true))
  }

  return (
    <ReactPlayer
      controls={false}
      pip={true}
      id={'VideoPlayer'}
      ref={videoPlayerRef}
      url={videoUrl}
      style={{
        height: '100%',
        flex: '1 0 auto',
        position: 'relative',
      }}
      width="100%"
      height="100%"
      volume={0}
      playing={isPlaying}
      onPause={onPause}
      onPlay={onPlay}
    />
  )
}

export default VideoPlayer
