import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getCurrentAudio, getIsPlaying } from '../../../utils/Selectors'
import AudioApi, { FormatsRes } from '../../../api/AudioApi'
import { setPlaying } from '../playerSlice'

const VideoPlayerCore = () => {
  const dispatch = useDispatch()
  const currentAudio = useSelector(getCurrentAudio)
  const isPlaying = useSelector(getIsPlaying)

  const videoRef = useRef<HTMLVideoElement>(null)

  useEffect(() => {
    let vid: HTMLVideoElement = document.getElementById(
      'videoCore'
    ) as HTMLVideoElement
    let aud: HTMLAudioElement = document.getElementById(
      'player'
    ) as HTMLAudioElement

    aud.currentTime = vid.currentTime
    aud.play()
  }, [])

  // useEffect(() => {
  //   if (videoRef?.current) {
  //     const audioPlayer: HTMLAudioElement = document.getElementById(
  //       'player'
  //     ) as HTMLAudioElement
  //     audioPlayer.currentTime = videoRef.current.currentTime
  //     console.log('true')
  //   }
  // }, [videoRef, isPlaying])

  const [videoUrl, setVideoUrl] = useState<string>('')

  useEffect(() => {
    if (currentAudio.id) {
      AudioApi.getVideoFormats(currentAudio.id).then((res: FormatsRes[]) => {
        const video = res.pop()
        if (video) {
          setVideoUrl(video.url)
        }
      })
    }
  }, [currentAudio])

  useEffect(() => {
    if (videoRef?.current) {
      if (isPlaying) {
        videoRef.current.play().then(null)
      } else {
        videoRef.current.pause()
      }
    }
  }, [isPlaying, videoRef])

  const onPause = useCallback(() => {
    dispatch(setPlaying(false))
  }, [])
  const onPlay = useCallback(() => {
    dispatch(setPlaying(true))
  }, [])

  return (
    <video
      ref={videoRef}
      id={'videoCore'}
      src={videoUrl}
      controls
      style={{
        height: '100%',
        flex: '1 0 auto',
        position: 'relative',
      }}
    />
  )
}

export default VideoPlayerCore
