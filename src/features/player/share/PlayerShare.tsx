import React, { useEffect } from 'react'
import PlayerFullScreenLayout from '../playerFullScreen/PlayerFullScreenLayout'
import { useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { setAudio, setIsVideo, setPlaying } from '../playerSlice'
import { getIsStateRestored } from '../../../utils/Selectors'

const PlayerShare = () => {
  const { audio } = useParams<{ audio: string }>()

  const isStateRestored = useSelector(getIsStateRestored)

  const dispatch = useDispatch()
  useEffect(() => {
    if (audio && isStateRestored) {
      const sharedAudio = JSON.parse(decodeURIComponent(audio))
      if (sharedAudio) {
        dispatch(setAudio(sharedAudio))
        dispatch(setPlaying(false))
        dispatch(setIsVideo(true))
      }
    }
  }, [audio, isStateRestored, dispatch])

  return <PlayerFullScreenLayout />
}

export default PlayerShare
