import { Audio, Player, Playlist } from './types'
import { createSlice, PayloadAction } from '@reduxjs/toolkit'

const room = Math.round(Math.random() * 10000000)

console.log('=====YOU ROOM IS =====', room)

const initialState: Player = {
  currentAudio: {
    type: 'audio',
    id: '',
    name: '',
    artist: '',
    url: '',
    thumbnail: '',
    maxTime: 0,
  },
  nextAudio: {
    type: 'audio',
    id: '',
    name: '',
    artist: '',
    url: '',
    thumbnail: '',
    maxTime: 0,
  },
  prevAudio: {
    type: 'audio',
    id: '',
    name: '',
    artist: '',
    url: '',
    thumbnail: '',
    maxTime: 0,
  },
  playlist: {
    id: '',
    name: '',
    url: '',
    audios: [],
    isDynamic: false,
  },
  isShuffle: false,
  isPlaying: false,
  isLoop: false,
  volume: 100,
  currentTime: 0,
  isVideo: false,
  videoPlayerRef: null,

  isStateRestored: false,
  isOverlayOpen: false,
  room: room,
  name: room,
}

const playerSlice = createSlice({
  name: 'player',
  initialState,
  reducers: {
    setCurrentTime(state, action: PayloadAction<number>) {
      state.currentTime = action.payload
    },
    setPlayerState(state, action: PayloadAction<Player>) {
      state.currentAudio = action.payload.currentAudio
      state.nextAudio = action.payload.nextAudio
      state.prevAudio = action.payload.prevAudio
      state.playlist = action.payload.playlist
      state.isShuffle = action.payload.isShuffle
      state.isPlaying = action.payload.isPlaying
      state.isLoop = action.payload.isLoop
      state.volume = action.payload.volume
      state.currentTime = action.payload.currentTime
      state.isStateRestored = true
    },
    setAudio(state, action: PayloadAction<Audio>) {
      if (state.currentAudio.id !== action.payload.id) {
        state.currentAudio = action.payload
        state.isPlaying = true
        state.isVideo =
          action.payload.url.includes('youtube') ||
          Boolean(action.payload.videoUrl)

        state.currentTime = 0

        const { next, prev } = getNextPrev(
          action.payload,
          state.playlist.audios
        )
        state.nextAudio = next
        state.prevAudio = prev
      } else {
        state.isPlaying = !state.isPlaying
      }
    },
    setVideo(state, action: PayloadAction<string>) {
      state.currentAudio.videoUrl = action.payload
      state.isVideo = true
    },
    setPlaylist(
      state,
      action: PayloadAction<{ playlist: Playlist; audio: Audio }>
    ) {
      state.currentAudio = action.payload.audio
      state.playlist = action.payload.playlist
      state.isPlaying = true
      state.isVideo =
        action.payload.audio.url.includes('youtube') ||
        Boolean(action.payload.audio.videoUrl)

      const { next, prev } = getNextPrev(
        action.payload.audio,
        action.payload.playlist.audios
      )
      state.nextAudio = next
      state.prevAudio = prev
    },
    playNext(state) {
      let idx = getAudioIdx(state.nextAudio, state.playlist.audios)
      idx = idx + 1 < state.playlist.audios.length ? idx + 1 : 0
      state.prevAudio = state.currentAudio
      state.currentAudio = state.nextAudio
      state.isVideo =
        state.nextAudio.url.includes('youtube') ||
        Boolean(state.nextAudio.videoUrl)

      state.nextAudio = state.playlist.audios[idx]
      state.isPlaying = true
      state.currentTime = 0
    },
    playPrev(state) {
      let idx = getAudioIdx(state.prevAudio, state.playlist.audios)
      idx = idx > 0 ? idx - 1 : state.playlist.audios.length - 1
      state.nextAudio = state.currentAudio
      state.currentAudio = state.prevAudio
      state.isVideo =
        state.prevAudio.url.includes('youtube') ||
        Boolean(state.prevAudio.videoUrl)
      state.prevAudio = state.playlist.audios[idx]
      state.isPlaying = true
      state.currentTime = 0
    },
    setPlaying(state, action: PayloadAction<boolean>) {
      state.isPlaying = action.payload
    },
    setLoop(state, action: PayloadAction<boolean>) {
      state.isLoop = action.payload
    },
    setShuffle(state, action: PayloadAction<boolean>) {
      state.isShuffle = action.payload
    },
    setVolume(state, action: PayloadAction<number>) {
      state.volume = action.payload
    },
    setIsVideo(state, action: PayloadAction<boolean>) {
      state.isVideo = action.payload
    },
    setVideoPlayerRef(state, action: PayloadAction<any>) {
      state.videoPlayerRef = action.payload
    },
    setIsOverlayOpen(state, action: PayloadAction<boolean>) {
      state.isOverlayOpen = action.payload
    },
    setName(state, action: PayloadAction<number>) {
      state.name = action.payload
    },
    setRoom(state, action: PayloadAction<number>) {
      state.room = action.payload
    },
  },
})

const getAudioIdx = (audio: Audio, audios: Audio[]): number => {
  return audios.findIndex((el) => el.id === audio.id)
}

const getNextPrev = (audio: Audio, audios: Audio[]) => {
  let idx = getAudioIdx(audio, audios)
  const nextIdx = idx + 1 < audios.length ? idx + 1 : 0
  const prevIdx = idx > 0 ? idx - 1 : audios.length - 1
  return { next: audios[nextIdx], prev: audios[prevIdx] }
}

export const {
  setCurrentTime,
  setPlayerState,
  setAudio,
  setPlaylist,
  playNext,
  playPrev,
  setPlaying,
  setLoop,
  setShuffle,
  setVolume,
  setIsVideo,
  setVideo,
  setVideoPlayerRef,
  setIsOverlayOpen,
  setName,
  setRoom,
} = playerSlice.actions

export default playerSlice.reducer
