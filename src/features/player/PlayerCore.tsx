import React, { useRef } from 'react'
import { initDB } from 'react-indexed-db'
import { DBConfig } from '../../utils/IndexedDB'
import useReduxPlayer from './hooks/useReduxPlayer'

initDB(DBConfig)

const PlayerCore = () => {
  const playerRef = useRef<HTMLAudioElement>(null)

  const { isLoop, url } = useReduxPlayer(playerRef)

  return (
    <audio
      id={'player'}
      preload={'auto'}
      loop={isLoop}
      src={url}
      ref={playerRef}
    />
  )
}

export default PlayerCore
