export type Audio = {
  type: 'audio'
  id: string
  name: string
  artist: string
  url: string
  thumbnail: string
  maxTime: number
  videoUrl?: string
}

export type Playlist = {
  id: string
  url: string
  audios: Audio[]
  name: string
  isDynamic: boolean
}

export type Player = {
  currentAudio: Audio
  nextAudio: Audio
  prevAudio: Audio
  isPlaying: boolean
  isShuffle: boolean
  isLoop: boolean
  playlist: Playlist
  volume: number
  currentTime: number

  isVideo: boolean
  videoPlayerRef: any

  isStateRestored: boolean
  isOverlayOpen: boolean

  room: number
  name: number
}
