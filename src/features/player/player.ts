import { Audio, Playlist } from './types'
import { makeAutoObservable } from 'mobx'

const room = Math.round(Math.random() * 10000000)

console.log('=====YOU ROOM IS =====', room)

class Player {
  currentAudio: Audio = {
    type: 'audio',
    id: '',
    name: '',
    artist: '',
    url: '',
    thumbnail: '',
    maxTime: 0,
  }

  nextAudio: Audio = {
    type: 'audio',
    id: '',
    name: '',
    artist: '',
    url: '',
    thumbnail: '',
    maxTime: 0,
  }

  prevAudio: Audio = {
    type: 'audio',
    id: '',
    name: '',
    artist: '',
    url: '',
    thumbnail: '',
    maxTime: 0,
  }

  playlist: Playlist = {
    id: '',
    name: '',
    url: '',
    audios: [],
    isDynamic: false,
  }

  isShuffle: boolean = false
  isPlaying: boolean = false
  isLoop: boolean = false
  volume: number = 100
  currentTime: number = 0
  isVideo: boolean = false
  videoPlayerRef: any = null

  isStateRestored: boolean = false
  isOverlayOpen: boolean = false
  room: number = room
  name: number = room

  constructor() {
    makeAutoObservable(this)
  }

  setCurrentTime(time: number) {
    this.currentTime = time
  }

  setAudio(audio: Audio) {
    if (this.currentAudio.id !== audio.id) {
      this.currentAudio = audio
      this.isPlaying = true
      this.isVideo = audio.url.includes('youtube') || Boolean(audio.videoUrl)

      this.currentTime = 0

      const { next, prev } = getNextPrev(audio, this.playlist.audios)
      this.nextAudio = next
      this.prevAudio = prev
    } else {
      this.isPlaying = !this.isPlaying
    }
  }

  setVideo(url: string) {
    this.currentAudio.videoUrl = url
    this.isVideo = true
  }

  setPlaylist(playlist: { playlist: Playlist; audio: Audio }) {
    this.currentAudio = playlist.audio
    this.playlist = playlist.playlist
    this.isPlaying = true
    this.isVideo =
      playlist.audio.url.includes('youtube') || Boolean(playlist.audio.videoUrl)

    const { next, prev } = getNextPrev(playlist.audio, playlist.playlist.audios)
    this.nextAudio = next
    this.prevAudio = prev
  }
  playNext() {
    let idx = getAudioIdx(this.nextAudio, this.playlist.audios)
    idx = idx + 1 < this.playlist.audios.length ? idx + 1 : 0
    this.prevAudio = this.currentAudio
    this.currentAudio = this.nextAudio
    this.isVideo =
      this.nextAudio.url.includes('youtube') || Boolean(this.nextAudio.videoUrl)

    this.nextAudio = this.playlist.audios[idx]
    this.isPlaying = true
    this.currentTime = 0
  }
  playPrev() {
    let idx = getAudioIdx(this.prevAudio, this.playlist.audios)
    idx = idx > 0 ? idx - 1 : this.playlist.audios.length - 1
    this.nextAudio = this.currentAudio
    this.currentAudio = this.prevAudio
    this.isVideo =
      this.prevAudio.url.includes('youtube') || Boolean(this.prevAudio.videoUrl)
    this.prevAudio = this.playlist.audios[idx]
    this.isPlaying = true
    this.currentTime = 0
  }

  setPlaying(isPlaying: boolean) {
    this.isPlaying = isPlaying
  }

  setLoop(isLoop: boolean) {
    this.isLoop = isLoop
  }
  setShuffle(isShuffle: boolean) {
    this.isShuffle = isShuffle
  }
  setVolume(volume: number) {
    this.volume = volume
  }
  setIsVideo(isVideo: boolean) {
    this.isVideo = isVideo
  }
  setVideoPlayerRef(ref: any) {
    this.videoPlayerRef = ref
  }
  setIsOverlayOpen(isOverlayOpen: boolean) {
    this.isOverlayOpen = isOverlayOpen
  }
  setName(name: number) {
    this.name = name
  }
  setRoom(room: number) {
    this.room = room
  }
}

const getAudioIdx = (audio: Audio, audios: Audio[]): number => {
  return audios.findIndex((el) => el.id === audio.id)
}

const getNextPrev = (audio: Audio, audios: Audio[]) => {
  let idx = getAudioIdx(audio, audios)
  const nextIdx = idx + 1 < audios.length ? idx + 1 : 0
  const prevIdx = idx > 0 ? idx - 1 : audios.length - 1
  return { next: audios[nextIdx], prev: audios[prevIdx] }
}

export default new Player()
