import AuthApi, { LoginDetails } from '../../api/AuthApi'
import ProfileApi from '../../api/ProfileApi'
import { makeAutoObservable } from 'mobx'

enum Status {
  'Authorized',
  'Unauthorized',
}

class Auth {
  username: string = ''
  status: Status = Status.Unauthorized
  isLoading: boolean = false

  constructor() {
    makeAutoObservable(this)
  }

  get isAuth() {
    return this.status === Status.Authorized
  }

  async login(loginDetails: LoginDetails) {
    this.isLoading = true

    const authResponse = await AuthApi.login(loginDetails)
    this.isLoading = false

    this.status = authResponse ? Status.Authorized : Status.Unauthorized
  }

  logout() {
    AuthApi.logout().then(() => {
      this.status = Status.Unauthorized
    })
  }

  async checkStatus() {
    const statusResponse = await ProfileApi.checkStatus()

    this.status = statusResponse ? Status.Authorized : Status.Unauthorized
  }
}

export default new Auth()
