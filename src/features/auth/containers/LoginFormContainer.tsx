import { reduxForm } from 'redux-form'
import LoginForm, { ValidateValues } from '../components/LoginForm'

const validate = (values: ValidateValues) => {
  const errors = { username: '', password: '' }

  return errors
}

export default reduxForm({ form: 'login', validate })(LoginForm)
