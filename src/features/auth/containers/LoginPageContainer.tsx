import React, { useEffect } from 'react'
import { User } from '../../../api/ProfileApi'
import { Redirect } from 'react-router-dom'
import LoginPage from '../components/LoginPage'
import authStore from '../auth'
import { observer } from 'mobx-react'

const LoginPageContainer = observer(() => {
  useEffect(() => {
    authStore.checkStatus()
  }, [])

  const onSubmit = (user: User) => {
    authStore.login(user)
  }

  console.log(authStore.isAuth)

  return (
    <>
      {authStore.isAuth ? (
        <Redirect to="/" />
      ) : (
        <LoginPage onSubmit={onSubmit} />
      )}
    </>
  )
})

export default LoginPageContainer
