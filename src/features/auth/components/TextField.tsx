import React from 'react'
import { FilledInput, FormControl, InputLabel } from '@material-ui/core'

type TextFieldProps = {
  label: string
  input: any
  meta: { touched: boolean; invalid: boolean; error: boolean }
}

const TextField: React.FC<TextFieldProps> = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <FormControl variant="filled">
    <InputLabel>{label}</InputLabel>
    <FilledInput
      error={touched && invalid}
      helperText={touched && error}
      {...input}
      {...custom}
    />
  </FormControl>
)

export default TextField
