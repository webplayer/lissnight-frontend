import React, { useState } from 'react'
import styles from './LoginForm.module.scss'
import TextField from './TextField'
import { IconButton, InputAdornment } from '@material-ui/core'
import { Visibility, VisibilityOff } from '@material-ui/icons'
import { Field } from 'redux-form'
import { log } from 'util'

const PasswordInput = () => {
  const [isShowPass, setIsShowPass] = useState(false)
  const togglePassShow = () => {
    setIsShowPass(!isShowPass)
  }

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault()
  }

  return (
    <Field
      className={styles.Field}
      name="password"
      component={TextField}
      label="Пароль"
      type={isShowPass ? 'text' : 'password'}
      endAdornment={
        <InputAdornment position="end">
          <IconButton
            aria-label="toggle password visibility"
            onClick={togglePassShow}
            edge="end"
          >
            {isShowPass ? <Visibility /> : <VisibilityOff />}
          </IconButton>
        </InputAdornment>
      }
    />
  )
}

export default PasswordInput
