import styles from './LoginForm.module.scss'
import TextField from './TextField'
import { Field } from 'redux-form'
import React from 'react'

const LoginInput = () => {
  return (
    <Field
      className={styles.Field}
      name="username"
      component={TextField}
      label="Логин"
    />
  )
}

export default LoginInput
