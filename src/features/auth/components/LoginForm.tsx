import React from 'react'
import { Button } from '@material-ui/core'
import { InjectedFormProps } from 'redux-form'
import styles from './LoginForm.module.scss'
import LoginInput from './LoginInput'
import PasswordInput from './PasswordInput'

export type ValidateValues = {
  [x: string]: string
  username: string
  password: string
}

const LoginForm: React.FC<InjectedFormProps<ValidateValues>> = ({
  handleSubmit,
  pristine,
  submitting,
}) => {
  return (
    <form autoComplete={'off'} onSubmit={handleSubmit} className={styles.Form}>
      <LoginInput />
      <PasswordInput />
      <Button
        variant={'contained'}
        type="submit"
        disabled={pristine || submitting}
      >
        Отправить
      </Button>
    </form>
  )
}

export default LoginForm
