import React from 'react'
import { Container } from '@material-ui/core'
import styles from './LoginPage.module.scss'
import { User } from '../../../api/ProfileApi'
import LoginFormContainer from '../containers/LoginFormContainer'

type LoginPageProps = {
  onSubmit: (user: User) => void
}

const LoginPage: React.FC<LoginPageProps> = ({ onSubmit }) => {
  return (
    <Container className={styles.Container}>
      <LoginFormContainer onSubmit={onSubmit} />
    </Container>
  )
}

export default LoginPage
