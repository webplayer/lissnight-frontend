import React from 'react'
import { Row } from 'antd'

type CellsGridProps = {
  paddingTop?: string
}

const CellsGrid: React.FC<CellsGridProps> = ({
  children,
  paddingTop = '25px',
}) => {
  return (
    <Row justify="center">
      <Row
        style={{
          paddingTop,
          paddingBottom: '25px',
          flex: '1 1 auto',
        }}
        justify="center"
      >
        {children}
      </Row>
    </Row>
  )
}

export default CellsGrid
