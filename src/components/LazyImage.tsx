import React from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import logo from './LissNight.png'

type LazyImageProps = {
  src: string
  height: string
  alt: string
  className: string
}

const LazyImage: React.FC<LazyImageProps> = ({
  src,
  alt,
  className,
  ...rest
}) => {
  const onError = (e: React.SyntheticEvent<HTMLImageElement, Event>) => {
    e.currentTarget.onerror = null
    e.currentTarget.src = getDefaultImage()
  }

  return (
    <LazyLoadImage
      className={className}
      alt={alt}
      src={src}
      onError={onError}
      decoding={'async'}
      effect="opacity"
      threshold={200}
      placeholderSrc={getDefaultImage()}
      {...rest}
    />
  )
}

type SmartImageProps = JSX.IntrinsicAttributes &
  React.ClassAttributes<HTMLImageElement> &
  React.ImgHTMLAttributes<HTMLImageElement>

export const SmartImage: React.FC<SmartImageProps> = ({
  alt,
  style,
  ...rest
}) => {
  const onError = (e: React.SyntheticEvent<HTMLImageElement>) => {
    e.currentTarget.onerror = null
    e.currentTarget.src = getDefaultImage()
  }

  return (
    <img
      alt={alt}
      style={{ ...style, backgroundColor: 'black' }}
      onError={onError}
      {...rest}
    />
  )
}

export const getDefaultImage = () => {
  return logo
}

export default LazyImage
