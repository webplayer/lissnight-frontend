import React from 'react'
import './Brand.scss'
import { Link } from 'react-router-dom'
import logo from './LissNight.png'
import logoText from './LissNightText.png'

type OpenProps = {
  isOpen: boolean
}

const Brand: React.FC<OpenProps> = ({ isOpen }) => {
  return (
    <div className={'Brand'}>
      <Link to={'/'}>
        <img alt={''} height={50} width={50} src={logo} />
        {isOpen && (
          <img
            alt={''}
            src={logoText}
            height={30}
            style={{ paddingTop: '5px', width: '103px' }}
          />
        )}
      </Link>
    </div>
  )
}

export const MenuLogo = ({ isOpen = true }: OpenProps) => {
  return (
    <div className={'MenuLogo'}>
      <Brand isOpen={isOpen} />
    </div>
  )
}

export default Brand
