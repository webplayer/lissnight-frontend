import React, { useCallback, useEffect, useState } from 'react'
import styles from './MediaCell.module.scss'
import clsx from 'clsx'
import { SmartImage } from './LazyImage'
import { Col } from 'antd'
import logo from './LissNight.png'
import { useHistory } from 'react-router-dom'
import Helpers from '../utils/Helpers'

type MediaCellProps = {
  isPlaying: boolean
  thumbnail: string
  onButtonClick: () => void
  onCellClick: () => void
  info: string
  time?: number
  artist?: string
}

const MediaCell = ({
  isPlaying,
  thumbnail,
  info,
  onButtonClick,
  onCellClick,
  time = 0,
  artist = '',
}: MediaCellProps) => {
  const [wrapperClasses, setWrapperClasses] = useState(
    clsx([styles.ImageWrapper])
  )
  const [imageClasses, setImageClasses] = useState(clsx([styles.Image]))
  const [buttonClasses, setButtonClasses] = useState(clsx([styles.PlayButton]))
  // const [infoClasses, setInfoClasses] = useState(clsx([styles.AbsoluteInfo]))

  useEffect(() => {
    if (isPlaying) {
      setWrapperClasses(clsx([wrapperClasses, styles.ImageWrapperActive]))
      setImageClasses(clsx([imageClasses, styles.ImageActive]))
      setButtonClasses(clsx([buttonClasses, styles.PlayButtonActive]))
      // setInfoClasses(clsx([infoClasses, styles.AbsoluteInfoActive]))
    } else {
      setWrapperClasses(clsx([styles.ImageWrapper]))
      setImageClasses(clsx([styles.Image]))
      setButtonClasses(clsx([styles.PlayButton]))
      // setInfoClasses(clsx([styles.AbsoluteInfo]))
    }
    // eslint-disable-next-line
  }, [isPlaying])

  const history = useHistory()
  const onArtistClick = useCallback(
    (event) => {
      event.stopPropagation()
      history.push(`/search/${artist}`)
    },
    [artist, history]
  )

  return (
    <Col
      style={{ color: 'rgb(179, 179, 179)', maxWidth: '220px' }}
      xs={12}
      md={8}
      lg={6}
      xxl={4}
    >
      <div style={{ padding: '10px', overflow: 'hidden' }}>
        <div className={wrapperClasses}>
          <PlayButton
            className={buttonClasses}
            onClick={onButtonClick}
            isPlaying={isPlaying}
          />
          <SmartImage
            alt={''}
            className={imageClasses}
            src={thumbnail}
            draggable={false}
            onClick={onCellClick}
          />
          <div className={styles.AbsoluteInfo} onClick={onCellClick}>
            <div title={info} className={styles.Info + ' ellipsis-one-line'}>
              {info}
            </div>
            {artist || time ? (
              <div className={styles.HelpInfo}>
                {time && (
                  <div className={styles.TimeInfo}>
                    {Helpers.formatTime(time)}
                  </div>
                )}
                {artist && (
                  <div
                    title={artist}
                    className={styles.ArtistInfo + ' ellipsis-one-line'}
                    onClick={onArtistClick}
                  >
                    {artist}
                  </div>
                )}
              </div>
            ) : (
              <></>
            )}
          </div>
        </div>
      </div>
    </Col>
  )
}

type PlayButtonProps = {
  isPlaying: boolean
  onClick: () => void
  className: string
}
const PlayButton = ({ isPlaying, onClick, className }: PlayButtonProps) => {
  return (
    <div className={className} onClick={onClick}>
      <div style={{ userSelect: 'none' }}>
        {isPlaying ? (
          <img alt={''} src={logo} width={'70%'} height={'70%'} />
        ) : (
          '|>'
        )}
      </div>
    </div>
  )
}

export default MediaCell
