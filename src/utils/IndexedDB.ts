export const DBConfig = {
  name: 'AppDB',
  version: 1.1,
  objectStoresMeta: [
    {
      store: 'users',
      storeConfig: {
        keyPath: 'id',
        autoIncrement: true,
      },
      storeSchema: [
        { name: 'name', keypath: 'name', options: { unique: true } },
        {
          name: 'playerState',
          keypath: 'playerState',
          options: { unique: false },
        },
      ],
    },
  ],
}
