import SearchApi from '../api/SearchApi'
import moment from 'moment'

const Helpers = {
  urlToHttps: (url: string) => {
    let httpsUrl = url.replace('http:', 'https:')
    if (url.substr(0, 2) === '//') {
      httpsUrl = `https:${url}`
    }
    return httpsUrl
  },
  toUniqueArray: (array: any[]) => {
    // @ts-ignore
    return [...new Set(array.map(JSON.stringify))].map(
      // @ts-ignore
      JSON.parse
    )
  },
  splitArray: (array: any[], pageSize: number) => {
    let pages = []
    for (let i = 0; i < Math.ceil(array.length / pageSize); i++) {
      pages[i] = array.slice(i * pageSize, i * pageSize + pageSize)
    }
    return pages
  },
  getPages: (array: any[], pageSize: number, pageAmount: number) => {
    return array.slice(0, pageAmount * pageSize)
  },
  findVideo: async (name: string, length: number) => {
    const videoPage = (await SearchApi.search(name, 'youtube')).data.filter(
      (el: { type: string }) => el.type === 'audio'
    )

    const compare = (el1: { timeDiff: number }, el2: { timeDiff: number }) => {
      return el1.timeDiff - el2.timeDiff
    }

    const timeDifArr = videoPage
      .map((el: { id: string; maxTime: number }) => ({
        id: el.id,
        timeDiff: el.maxTime - length,
      }))
      .filter((el: { timeDiff: number }) => el.timeDiff >= 0)
      .sort(compare)
    return timeDifArr[0]
  },
  formatTime: (time: number | number[]) => {
    let format = 'm:ss'
    if (time >= 600) {
      format = 'mm:ss'
    }
    if (time >= 3600) {
      format = 'H:mm:ss'
    }
    if (time >= 36000) {
      format = 'HH:mm:ss'
    }
    if (typeof time === 'number') {
      if (time < 0) {
        return moment.utc(0).format(format)
      }
      return moment.utc(time * 1000).format(format)
    }
    return time.toString()
  },
}

export default Helpers
