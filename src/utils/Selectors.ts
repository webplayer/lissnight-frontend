import { createSelector } from 'reselect'
import { RootState } from '../app/reducer'

export const getName = (state: RootState) => state.player.name
export const getRoom = (state: RootState) => state.player.room
export const getCurrentAudio = (state: RootState) => state.player.currentAudio
export const getCurrentAudioId = (state: RootState) =>
  state.player.currentAudio.id
export const getCurrentAudioUrl = (state: RootState) =>
  state.player.currentAudio.url
export const getCurrentAudioVideoUrl = (state: RootState) =>
  state.player.currentAudio.videoUrl
export const getIsPlaying = (state: RootState) => state.player.isPlaying
export const getIsLoop = (state: RootState) => state.player.isLoop
export const getVolume = (state: RootState) => state.player.volume
export const getPlayerState = (state: RootState) => state.player
export const getCurrentTime = (state: RootState) => state.player.currentTime
export const getIsOverlayOpen = (state: RootState) => state.player.isOverlayOpen
export const getCurrentPlaylistId = (state: RootState) =>
  state.player.playlist.id
export const getIsStateRestored = (state: RootState) =>
  state.player.isStateRestored
export const getIsVideo = (state: RootState) => state.player.isVideo
export const getIsPlaylistEmpty = (state: RootState) => {
  return !Boolean(state.player.playlist.audios.length)
}
export const getCurrentPlaylist = (state: RootState) => state.player.playlist
export const getCurrentPlaylistAudios = (state: RootState) =>
  state.player.playlist.audios
export const getIsShuffle = (state: RootState) => state.player.isShuffle
export const getMediaLibraryDynamic = (state: RootState) =>
  state.mediaLibrary.dynamic
export const getMediaLibraryStatic = (state: RootState) =>
  state.mediaLibrary.static
export const getIsSearchFetching = (state: RootState) => state.search.isFetching
export const getSearchNextUrl = (state: RootState) =>
  state.search.searchResult.nextPageUrl
export const getSearchResult = (state: RootState) => state.search.searchResult
export const getSearchResultInput = (state: RootState) =>
  state.search.searchResult.searchInput
export const getAutocompleteList = (state: RootState) =>
  state.search.autocompleteList

const getTargetId = (_: RootState, id: string) => id
export const getIsTargetAudioPlaying = createSelector(
  getIsPlaying,
  getCurrentAudioId,
  getTargetId,
  (isPlaying, currentAudioId, targetAudioId) => {
    return isPlaying && currentAudioId === targetAudioId
  }
)
export const getSmartVideoUrl = createSelector(
  getCurrentAudioId,
  getCurrentAudioUrl,
  getCurrentAudioVideoUrl,
  (id, url, videoUrl) => {
    if (id && url.includes('youtube')) {
      return `${id}`
    } else if (videoUrl) {
      return `${videoUrl}`
    }
    return ''
  }
)
export const getMediaLibraryPlaylists = createSelector(
  getMediaLibraryDynamic,
  getMediaLibraryStatic,
  (dynamicPlaylists, staticPlaylists) => {
    return [...dynamicPlaylists, ...staticPlaylists]
  }
)
export const getStaticPlaylistById = createSelector(
  getMediaLibraryStatic,
  getTargetId,
  (staticPlaylists, id) => {
    return staticPlaylists.find((playlist) => playlist.id.toString() === id)
  }
)

const getTargetSource = (_: RootState, source: string) => source
export const getDynamicPlaylistBy = createSelector(
  getMediaLibraryDynamic,
  getTargetId,
  getTargetSource,
  (dynamicPlaylist, id, source) => {
    return dynamicPlaylist[-1] || dynamicPlaylist[0]
  }
)
