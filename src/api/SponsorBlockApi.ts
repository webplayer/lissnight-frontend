import axios from 'axios'

const SponsorBlockApi = {
  getSkipBlocks(videoId: string) {
    return axios.get(
      `https://sponsor.ajay.app/api/skipSegments?videoID=${videoId}&categories=["sponsor","intro","outro","selfpromo","interaction","music_offtopic"]`
    )
  },
}

export default SponsorBlockApi
