import { ApiConnect } from './config/ApiConnect'
import qs from 'qs'

export type User = {
  username: string
  password: string
}

export const ProfileApi = {
  login(user: User) {
    return ApiConnect.get(`/login?${qs.stringify(user)}`).then((res) => {
      return res
    })
  },
  checkStatus() {
    return ApiConnect.get(`/status`).then((res) => res.data)
  },
}

export default ProfileApi
