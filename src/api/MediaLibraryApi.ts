import { ApiConnect } from './config/ApiConnect'
import Helpers from '../utils/Helpers'
import { Audio } from '../features/player/types'

export type AudioApi = {
  id: string
  title: string
  url: string
  thumbnail: string
  length: number
  artist: string
  source: 'youtube' | 'vk'
}

export type MediaLibraryPlaylist = {
  dynamic: boolean
  external_id?: string
  id: string
  source?: string
  thumbnail: string
  title: string
  audios: Audio[]
}

export type MediaLibraryDynamicPlaylist = {
  dynamic: boolean
  external_id?: string
  id: string
  source?: string
  thumbnail: string
  title: string
  audios: Audio[]
  playlists?: MediaLibraryDynamicPlaylist[]
}

type MediaLibraryEl = {
  playlist: MediaLibraryPlaylist
}

export const MediaLibraryApi = {
  addAudio(playlistId: string, audioSource: string, audioId: string) {
    return ApiConnect.post(
      `/playlists/add/?id=${playlistId}&s=${audioSource}&audio_id=${audioId}`
    )
  },
  deleteAudio(playlistId: string, audioId: string) {
    return ApiConnect.delete(
      `/playlists/remove/?id=${playlistId}&audio=${audioId}`
    )
  },
  addUserPlaylist(title: string, thumbnail: string) {
    return ApiConnect.post(
      `/playlists/create/?title=${title}&thumbnail=${thumbnail}`
    )
  },
  addExternalPlaylist(externalId: string, source: string, title: string) {
    return ApiConnect.post(
      `/playlists/external?external_id=${externalId}&s=${source}&title=${encodeURI(
        title
      )}`
    )
  },
  deletePlaylist(playlistId: string) {
    return ApiConnect.delete(`/playlists/delete?id=${playlistId}`)
  },
  getMediaLibrary() {
    return ApiConnect.get(`/playlists/all`)
      .then((res) => {
        const playlists = res.data.map((el: MediaLibraryEl) => ({
          ...el.playlist,
          thumbnail: Helpers.urlToHttps(el.playlist.thumbnail),
          audios: [],
        }))
        return {
          dynamic: playlists.filter((el: MediaLibraryPlaylist) => el.dynamic),
          static: playlists.filter((el: MediaLibraryPlaylist) => !el.dynamic),
          favorite: [],
        }
      })
      .catch(() => {
        return {
          dynamic: [],
          static: [],
          favorite: [],
        }
      })
  },
  getDynamicPlaylist(id: string, source: 'vk' | 'youtube') {
    return ApiConnect.get(
      `/playlists/external?external_id=${id}&s=${source}`
    ).then((res) => {
      return {
        dynamic: true,
        id: String(id),
        external_id: id || '',
        thumbnail: res.data.info.thumbnail,
        title: res.data.info.title,
        audios: parseAudios(res),
        playlists:
          res.data.info.playlists &&
          res.data.info.playlists.map((el: { playlist: any }) => el.playlist),
        source: source,
      }
    })
  },
  getStaticPlaylist(id: string) {
    return ApiConnect.get(`/playlists/get?id=${id}`).then((res) => {
      return {
        dynamic: false,
        id: id,
        thumbnail: res.data.info.thumbnail,
        title: res.data.info.title,
        // size: res.data.info.size,
        audios: parseAudios(res),
      }
    })
  },
}

const parseAudios = (res: any) => {
  return res.data.data
    .filter((el: { audio?: AudioApi }) => el.audio)
    .map((el: { audio?: AudioApi }) => {
      if (el.audio) {
        const audioUrl =
          ApiConnect.defaults.baseURL +
          'redirect?s=' +
          el.audio.source +
          '&id=' +
          el.audio.id
        return {
          type: 'audio',
          id: el.audio.id,
          name: el.audio.title,
          url: audioUrl,
          thumbnail: Helpers.urlToHttps(el.audio.thumbnail || ''),
          maxTime: el.audio.length,
          artist: el.audio.artist,
        }
      }
      return {}
    })
}

export default MediaLibraryApi
