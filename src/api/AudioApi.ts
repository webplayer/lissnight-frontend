import { ApiConnect } from './config/ApiConnect'
export type FormatsRes = { mime: string; quality: string; url: string }

const AudioApi = {
  getVideoFormats(id: string, source: string = 'youtube') {
    return ApiConnect.get(`/formats?s=${source}&id=${id}`).then((res) => {
      return res.data
        .filter((el: FormatsRes) => el.mime.startsWith('video/mp4'))
        .map((el: FormatsRes) => {
          return {
            quality: el.quality,
            url: el.url,
          }
        })
    })
  },
}
export default AudioApi
