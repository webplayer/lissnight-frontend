export let userName: string

const AudioStreamApi = new WebSocket('ws://192.168.0.4:3333/')

AudioStreamApi.onopen = () => {
  userName = prompt('Как вас зовут?') || ''
}

AudioStreamApi.onclose = () => {
  alert('Подключение окончено')
}

export default AudioStreamApi
