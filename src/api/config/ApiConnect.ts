import axios from 'axios'

const API_ENDPOINT = 'https://puvel.ru:8033/'
// const API_ENDPOINT = 'http://95.161.27.109:34197/'
// const API_ENDPOINT = 'http://192.168.0.5:8080/'

export const ApiConnect = axios.create({
  baseURL: API_ENDPOINT,
  withCredentials: true,
})
