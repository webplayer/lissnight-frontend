import { ApiConnect } from './config/ApiConnect'
import Helpers from '../utils/Helpers'
import { AxiosResponse } from 'axios'

export type ApiSearchResultEl = {
  audio?: Audio
  playlist?: Playlist
  channel?: Playlist
}

type Audio = {
  id: string
  title: string
  url: string
  thumbnail: string
  length: number
  artist: string
}

type Playlist = {
  size: number
  thumbnail: string
  title: string
  external_id: string
}

export const SearchApi = {
  search(q: string, source: string) {
    return ApiConnect.get(
      `/search?q=${encodeURIComponent(q)}&s=${source}`
    ).then(parseSearchResult)
  },
  searchNext(url: string) {
    return ApiConnect.get(encodeURI(url)).then(parseSearchResult)
  },
  autocomplete(q: string) {
    return ApiConnect.get(`/autocomplete?q=${encodeURI(q)}`).then(
      (res) => res.data
    )
  },
}

function parseSearchResult(res: AxiosResponse<any>) {
  return {
    nextSearchPage: res.data.next,
    data: res.data.data.map((el: ApiSearchResultEl) => {
      if (el.audio) {
        const audioUrl =
          ApiConnect.defaults.baseURL +
          'redirect?s=' +
          'youtube' +
          '&id=' +
          el.audio.id
        return {
          type: 'audio',
          id: el.audio.id,
          name: el.audio.title,
          url: audioUrl,
          thumbnail: Helpers.urlToHttps(el.audio.thumbnail),
          maxTime: el.audio.length,
          artist: el.audio.artist,
        }
      } else if (el.playlist) {
        return {
          type: 'playlist',
          size: el.playlist.size,
          id: el.playlist.external_id,
          name: el.playlist.title,
          url: el.playlist.external_id,
          thumbnail: Helpers.urlToHttps(el.playlist.thumbnail),
        }
      }
      return {}
    }),
  }
}

export default SearchApi
