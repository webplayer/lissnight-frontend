import { ApiConnect } from './config/ApiConnect'
import * as qs from 'qs'

export type LoginDetails = {
  username: string
  password: string
}

export const AuthApi = {
  login(loginDetails: LoginDetails) {
    return ApiConnect.get(`/login?${qs.stringify(loginDetails)}`)
  },

  logout() {
    return ApiConnect.delete('/login')
  },

  register(username: string, password: string) {
    const user = {
      username,
      password,
    }
    return ApiConnect.post(`/register?${qs.stringify(user)}`)
  },

  checkAuth() {
    return ApiConnect.get('/status')
  },
}

export default AuthApi
